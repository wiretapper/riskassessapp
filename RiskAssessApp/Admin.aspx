﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="RiskAssessApp.Admin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



        <script type="text/javascript">

        function pageLoad(sender, args) {
           

            $('#NewAppButn').click(function () {
               // alert('clicked button');
                ////working: $("#NewAppSlider").dialog('open');
                
                $("#NewAppSlider").css({ "margin-top": "80px" });
                
               $("#NewAppSlider").slideToggle().parent().appendTo(jQuery("form:first"));

               // jQuery("form:first").after("#NewAppSlider").slideToggle();

            });
            



          //  alert('hello from pageLoad jquery');
            
       

            $('.more-icon').toggleClass('less-icon');

            //prevent 'enter' key from triggering submit & clearing out the textboxes (default behaviour)
            //unless it's coming from the search boxes, then trigger the search.
            $('input').keypress(function (e) {  // $('form').keypress(function (e) {

                var code = e.keyCode || e.which;
                if (code === 13) {

                    var srchbtn = document.getElementById('MainContent_btnSrch')
                    var id = $(this).attr('id'); //hitting return triggers search if we're in any of the search boxes
                    //alert(id);
                    if (id === "MainContent_txtMRN1") {
                        e.preventDefault(); //stop the default 'clear' button from triggering
                        srchbtn.click();
                        //alert('mrn!');
                    } else if (id === "MainContent_txtFirstname1") {
                        e.preventDefault(); //stop the default 'clear' button from triggering
                        srchbtn.click();
                        //alert('first name!');
                    } else if (id === "MainContent_txtSurname1") {
                        //alert('surname!'); 
                        e.preventDefault(); //stop the default 'clear' button from triggering
                        srchbtn.click();
                    } else {
                        e.preventDefault(); // don't trigger search or postback if not in one of the above search boxes
                        return false;
                    }



                }


            })

            //note use of 'unbind' this stops the yoyo'ing when fv paging postbacks fire this pageLoad for each fv page no. click in breadcrumb.
            $("#expndr").unbind().click(function () {  //'toggle' results display and the bootstrap chevron 
                $('.divsrchResults').slideToggle(1000);
                $('#expndr').toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-right');
            });

            //grab the id of the collapsible section showing and stuff it into hidden field for reading & setting after postback
            $(".collapse").on('show.bs.collapse', function () {
                //  alert('opening ' + this.id);
                $("input[id=sectionOpn]").val(this.id);

            });

            //open the collapsible based on the hidden field after postback
            var opntab = $("input[id=sectionOpn]").val();

            //alert('in pl, about to open ' + opntab);

            if (opntab == "fvRefcolapse") {
                $('#fvRefcolapse').attr("class", "in");
            }

            //if (opntab == "fvBloodscolapse") {
            //    $('#fvBloodscolapse').attr("class", "in");
            //}
            //if (opntab == "fvAnalgcolapse") {
            //    $('#fvAnalgcolapse').attr("class", "in");
            //}

            //rebind calendar controls (also in site master>doc ready )
            $(function () {

                $('#txtDatRiskAss').datepicker({ dateFormat: 'dd/mm/yy' });
                $('#txtDatICTInfrmd').datepicker({ dateFormat: 'dd/mm/yy' });
                
            });


            //search button clicked, load spinner until grid is populated. 
            //nb: it stops automatically when grid is pop'd..prob due to postback
            $("#MainContent_btnSrch").click(function () {
                //  alert('clicked srch');
                $('#mySpinner').addClass('spinner');
            });


            ////clear search boxes
            $("#btnClrSrch").click(function () {
                event.preventDefault();
                $('#MainContent_txtMRN1').val('');
                $('#MainContent_txtFirstname1').val('');
                $('#MainContent_txtSurname1').val('');

                //alert('clicked clear');

            });


            // jquery dialog/alerts
            $(function () {

                $("#dialog").dialog({
                    autoOpen: false,
                    title: 'Note',
                    show: { effect: "fade", duration: 250 },
                    hide: { effect: "fade", duration: 1000 },
                    open: function (event, ui) {
                        setTimeout("$('#dialog').dialog('close')", 1500);
                    }
                });
            });

            function showDlg(txt) {
                $('#dialog').text(txt); //set the text programmatically
                $('#dialog').dialog('open');
            }


            // Ajax call to get notes history via button click  (called page load in a special aspx page to fetch records)

            //    $("#btnNotesHx").click(function () { 

            //        //Get the PIN from hidden field
            //        var PIN = $("input[id=fvPIN]").val();
            //        //alert('current PIN is: ' + PIN);

            //            $.ajax({
            //                contentType: "text/html; charset=utf-8", 
            //                data: "PIN= " +PIN, // "PIN= 946229", //posted to page in querystring for processing on aspx PageLoad in GetNoteHistoryAjx.aspx
            //                //data: "PxID=" + $('#Customers').val(),
            //                url: "GetNoteHistoryAjx.aspx",
            //                type: "GET",
            //                dataType: "html",
            //                success: function (data) {
            //                    $("#dvNotesHx").html(data).dialog("open");
            //                },
            //                error: function () {
            //                    $("#dvNotesHx").text('ajax call failed');
            //                }
            //            });
            //        //} //end if
            //}); //end click func


            //make the notes div a jquery popup
            //$(function () {
            //    $("#dvNotesHx").dialog({
            //        autoOpen: false,
            //        width: 500,
            //        modal: true,
            //        show: {
            //            effect: "fadeIn",
            //            duration: 500
            //        },
            //        hide: {
            //            effect: "fadeOut",
            //            duration: 500
            //        }
            //    }).css("font-size","1.3em");
            //});

            //var refdat = $("#RefDatTextBox").val(); //grab the value from one textbox for display in the one below:
            //$('#lblRefDat').html('  ...for Ref. Date: ' + refdat).css({ "font-size": ".9em", "background-color": "none !important", "color": "silver" })


        }; //end pageLoad

        function showDlg(txt) {
            $('#dialog').text(txt); //set the text programmatically
            $('#dialog').dialog('open');
        }

    </script>

        <%--popup jquery alert markup--%>
    <div id="dialog" style="font-size:1.3em"></div>



</asp:Content>
