﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RiskAssessApp.Startup))]
namespace RiskAssessApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
