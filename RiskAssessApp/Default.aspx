﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="RiskAssessApp._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <script type="text/javascript">

        function pageLoad(sender, args) {

         //// working:
         ////var NASlide =  $("#NewAppSlider").dialog ({

         ////       autoOpen: false,
         ////       modal: true,
         ////       width:400,
         ////       title: 'New Application',
         ////       show: { effect: "fade", duration: 300 },
         ////       hide: { effect: "fade", duration: 500 },
         ////       open: function (event, ui) {
         ////           //setTimeout("$('#NewAppSlider').dialog('close')", 2500);
         ////       },
         ////       buttons: {
         ////           "OK": function () {
         ////               $(this).dialog("close");
         ////           }
         ////       }
         ////   });

        //// working: 
        //// NASlide.parent().appendTo(jQuery("form:first")); //ensures the dialog above is appended inside form


            // collapsible section - all sections are open by default. 
            // if the app category is 'spreadsheets' or 'reports' then hide all except data protection section

            //1. store the opened collapsible div in a hidden field:
            //$(".collapse").on('show.bs.collapse', function () {
            //    alert('opening ' + this.id);
            //    $("input[id=sectionOpn]").val(this.id);
            //});

            //2. set the collapsible based on the hidden field:
            var opntab = $("input[id=sectionOpn]").val();

            //if (opntab == "") {
            //    alert(opntab + ' is nothing');
            //}

            if (opntab == "Spreadsheets" || opntab == "Reports" || opntab == "External Data Communication") {

                //alert('in check for spread or reports. opntab is: ' + opntab);

                $('#secA').attr("class", "collapse").fadeOut(1200); //setting class 'in' opens the collapsible div
                $('#secB').attr("class", "collapse").fadeOut(1200);
                $('#secC').attr("class", "collapse").fadeOut(1200);
                $('#secD').attr("class", "collapse").fadeIn(1200).attr("class", "in");
                $('#secE').attr("class", "collapse").fadeIn(1200).attr("class", "in");
            } else {
                //alert('in ELSE check for spread or reports. opntab is: ' + opntab);
                $('#secA').attr("class", "collapse").fadeIn(1200).attr("class", "in");
                $('#secB').attr("class", "collapse").fadeIn(1200).attr("class", "in");
                $('#secC').attr("class", "collapse").fadeIn(1200).attr("class", "in");
                $('#secD').attr("class", "collapse").fadeIn(1200).attr("class", "in");
                $('#secE').attr("class", "collapse").fadeIn(1200).attr("class", "in");

            }

            //alert('in pageLoad, about to open ' + opntab);


            $("#txtPt2RiskRat,#txtPt1RiskRat").keyup(function () {


            //check values in both boxes aren't >5
                if ($(this).val() <= 0 || ($(this).val() > 5 || isNaN($(this).val()))) {
                alert("please enter a value between 1 and 5 only");
                $(this).val("");
                return;
            }



                $('#txtOverallRiskRat').val($('#txtPt1RiskRat').val() * $('#txtPt2RiskRat').val());
                
                //todo: colour the overall risk rating box depending on values (now and at formload):
                // ranges: 1-5 green, 6-14 orange, 15+ red
                if ($('#txtOverallRiskRat').val() <= 5)
                {
                    $('#txtOverallRiskRat').css({ 'box-shadow': 'none', 'background': '#c2d69b', 'color': 'white' });
                }
                else if ($('#txtOverallRiskRat').val() > 5 && $('#txtOverallRiskRat').val() < 15)
                {              
                     $('#txtOverallRiskRat').css({ 'box-shadow': 'none', 'background': '#ffae36', 'color': 'white' });
                }
                else if ($('#txtOverallRiskRat').val() >= 15) {
                      $('#txtOverallRiskRat').css({ 'box-shadow': 'none', 'background': '#ff5252', 'color': 'white' });
                  }
                else {
                 $('#txtOverallRiskRat').css({ 'background': 'white', 'color': 'black' });
                }
                
            });



               // page load set overrall risk rating bg color:
                if ($('#txtOverallRiskRat').val() <= 5) {
                    $('#txtOverallRiskRat').css({ 'box-shadow': 'none', 'background': '#c2d69b', 'color': 'white' });
                }
                else if ($('#txtOverallRiskRat').val() > 5 && $('#txtOverallRiskRat').val() < 15) {
                    $('#txtOverallRiskRat').css({ 'box-shadow': 'none', 'background': '#ffae36', 'color': 'white' });
                }
                else if ($('#txtOverallRiskRat').val() >= 15) {
                    $('#txtOverallRiskRat').css({ 'box-shadow': 'none', 'background': '#ff5252', 'color': 'white' });
                }
                else {
                    $('#txtOverallRiskRat').css({ 'background': 'white', 'color': 'black' });
                }
           

            $('#NewAppButn').click(function () {
               // alert('clicked button');
                ////working: $("#NewAppSlider").dialog('open');
                
                $("#NewAppSlider").css({ "margin-top": "80px" });
                
               $("#NewAppSlider").slideToggle().parent().appendTo(jQuery("form:first"));

               // jQuery("form:first").after("#NewAppSlider").slideToggle();

            });
            


            $('#NewAppClosButn').click(function () {
                // alert('clicked button');
                ////working: $("#NewAppSlider").dialog('open');

                //$("#NewAppSlider").css({ "margin-top": "80px" });

                $("#NewAppSlider").slideToggle();

                // jQuery("form:first").after("#NewAppSlider").slideToggle();

            });

           
            
            $('#NewCompButn').click(function () {
                // alert('clicked button');
                ////working: $("#NewAppSlider").dialog('open');

                $("#NewCompSlider").css({ "margin-top": "10px" });

                $("#NewCompSlider").slideToggle().parent().appendTo(jQuery("form:first"));

                // jQuery("form:first").after("#NewAppSlider").slideToggle();

            });


            $('#NewCompClosButn').click(function () {
                // alert('clicked button');
                ////working: $("#NewAppSlider").dialog('open');

                //$("#NewAppSlider").css({ "margin-top": "80px" });

                $("#NewCompSlider").slideToggle();

                // jQuery("form:first").after("#NewAppSlider").slideToggle();

            });

          //  alert('hello from pageLoad jquery');
            
       

            $('.more-icon').toggleClass('less-icon');

            //prevent 'enter' key from triggering submit & clearing out the textboxes (default behaviour)
            //unless it's coming from the search boxes, then trigger the search.
            $('input').keypress(function (e) {  // $('form').keypress(function (e) {

                var code = e.keyCode || e.which;
                if (code === 13) {

                    var srchbtn = document.getElementById('MainContent_btnSrch')
                    var id = $(this).attr('id'); //hitting return triggers search if we're in any of the search boxes
                    //alert(id);
                    if (id === "MainContent_txtMRN1") {
                        e.preventDefault(); //stop the default 'clear' button from triggering
                        srchbtn.click();
                        //alert('mrn!');
                    } else if (id === "MainContent_txtFirstname1") {
                        e.preventDefault(); //stop the default 'clear' button from triggering
                        srchbtn.click();
                        //alert('first name!');
                    } else if (id === "MainContent_txtSurname1") {
                        //alert('surname!'); 
                        e.preventDefault(); //stop the default 'clear' button from triggering
                        srchbtn.click();
                    } else {
                        e.preventDefault(); // don't trigger search or postback if not in one of the above search boxes
                        return false;
                    }



                }


            })

            //note use of 'unbind' this stops the yoyo'ing when fv paging postbacks fire this pageLoad for each fv page no. click in breadcrumb.
            $("#expndr").unbind().click(function () {  //'toggle' results display and the bootstrap chevron 
                $('.divsrchResults').slideToggle(1000);
                $('#expndr').toggleClass('glyphicon-chevron-down').toggleClass('glyphicon-chevron-right');
            });

           //grab the id of the collapsible section showing and stuff it into hidden field for reading & setting after postback
            //$(".collapse").on('show.bs.collapse', function () {
            //    //  alert('opening ' + this.id);
            //    $("input[id=sectionOpn]").val(this.id);

            //});



            //if (opntab == "fvBloodscolapse") {
            //    $('#fvBloodscolapse').attr("class", "in");
            //}
            //if (opntab == "fvAnalgcolapse") {
            //    $('#fvAnalgcolapse').attr("class", "in");
            //}

            //rebind calendar controls (also in site master>doc ready )
            $(function () {

                $('#txtDatRiskAss').datepicker({ dateFormat: 'dd/mm/yy' });
                $('#txtDatICTInfrmd').datepicker({ dateFormat: 'dd/mm/yy' });
                
            });


            //search button clicked, load spinner until grid is populated. 
            //nb: it stops automatically when grid is pop'd..prob due to postback
            $("#MainContent_btnSrch").click(function () {
                //  alert('clicked srch');
                $('#mySpinner').addClass('spinner');
            });


            ////clear search boxes
            $("#btnClrSrch").click(function () {
                event.preventDefault();
                $('#MainContent_txtMRN1').val('');
                $('#MainContent_txtFirstname1').val('');
                $('#MainContent_txtSurname1').val('');

                //alert('clicked clear');

            });


            // jquery dialog/alerts
            $(function () {

                $("#dialog").dialog({
                    autoOpen: false,
                    title: 'Note',
                    show: { effect: "fade", duration: 250 },
                    hide: { effect: "fade", duration: 1000 },
                    open: function (event, ui) {
                        setTimeout("$('#dialog').dialog('close')", 1500);
                    }
                });
            });

            function showDlg(txt) {
                $('#dialog').text(txt); //set the text programmatically
                $('#dialog').dialog('open');
            }


            // Ajax call to get notes history via button click  (called page load in a special aspx page to fetch records)

            //    $("#btnNotesHx").click(function () { 

            //        //Get the PIN from hidden field
            //        var PIN = $("input[id=fvPIN]").val();
            //        //alert('current PIN is: ' + PIN);

            //            $.ajax({
            //                contentType: "text/html; charset=utf-8", 
            //                data: "PIN= " +PIN, // "PIN= 946229", //posted to page in querystring for processing on aspx PageLoad in GetNoteHistoryAjx.aspx
            //                //data: "PxID=" + $('#Customers').val(),
            //                url: "GetNoteHistoryAjx.aspx",
            //                type: "GET",
            //                dataType: "html",
            //                success: function (data) {
            //                    $("#dvNotesHx").html(data).dialog("open");
            //                },
            //                error: function () {
            //                    $("#dvNotesHx").text('ajax call failed');
            //                }
            //            });
            //        //} //end if
            //}); //end click func


            //make the notes div a jquery popup
            //$(function () {
            //    $("#dvNotesHx").dialog({
            //        autoOpen: false,
            //        width: 500,
            //        modal: true,
            //        show: {
            //            effect: "fadeIn",
            //            duration: 500
            //        },
            //        hide: {
            //            effect: "fadeOut",
            //            duration: 500
            //        }
            //    }).css("font-size","1.3em");
            //});

            //var refdat = $("#RefDatTextBox").val(); //grab the value from one textbox for display in the one below:
            //$('#lblRefDat').html('  ...for Ref. Date: ' + refdat).css({ "font-size": ".9em", "background-color": "none !important", "color": "silver" })


        }; //end pageLoad

        function showDlg(txt) {
            $('#dialog').text(txt); //set the text programmatically
            $('#dialog').dialog('open');
        }


    </script>

    <%--popup jquery alert markup--%>
    <div id="dialog" style="font-size:1.3em"></div>
   
    <%-- update panel for slider div with fv on it for new apps --%>

    <div id="NewAppSlider" class="collapse col-md-12" style="background-color: #f8f8f8; border: solid 1px silver; padding: 15px; border-radius: 3px; margin-bottom: 30px">
        <asp:UpdatePanel runat="server" ID="newAppUP" UpdateMode="Always" ClientIDMode="Static">
            <ContentTemplate>

                <asp:FormView ID="fvNewApp" ClientIDMode="Static" DataKeyNames="ID" runat="server" DefaultMode="Insert" OnItemCommand="fvNewApp_ItemCommand" OnItemInserting="fvNewApp_ItemInserting" OnItemUpdating="fvNewApp_ItemUpdating" OnModeChanging="fvNewApp_ModeChanging">
                    <EditItemTemplate>
                    </EditItemTemplate>

                    <InsertItemTemplate>
                        <div class="row">
                            <span class="LabelBoxCenter">Add New Item</span><br />
                            <span class="reqLabel">Company:</span>
                            <%--<asp:TextBox Text='<%# Bind("Company") %>' runat="server" ID="txtCompany" />--%>
                            <asp:DropDownList AppendDataBoundItems="true" AutoPostBack="false" OnInit="ddComp_Init" OnSelectedIndexChanged="ddComp_SelectedIndexChanged" runat="server" ID="ddComp">
                                <asp:ListItem Text="Please select" Value="" />
                            </asp:DropDownList>
                            <button type="button" id="NewCompButn"><span>&plus;</span></button>
                            <span class="reqLabel">Item Name:</span>
                            <asp:TextBox Text='<%# Bind("AppName") %>' runat="server" ID="txtAppName" />
                            <span class="reqLabel">Contact:</span>
                            <asp:TextBox Text='<%# Bind("ContactName") %>' runat="server" ID="txtContactName" />
                            <span class="reqLabel">Contact No.:</span>
                            <asp:TextBox Text='<%# Bind("ContactNo") %>' runat="server" ID="txtContactNo" />
                            <span class="reqLabel">Category:</span>
                            <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("Category") %>' runat="server" ID="ddCategory">
                                    <asp:ListItem Text="Please select" Value="" />                        
                                    <asp:ListItem>External Data Communication</asp:ListItem>
                                    <asp:ListItem>IT Application</asp:ListItem>
                                    <asp:ListItem>Interface</asp:ListItem>
                                    <asp:ListItem>Infrastructure</asp:ListItem>
                                    <asp:ListItem>Service</asp:ListItem>
                                    <asp:ListItem>Spreadsheets</asp:ListItem>
                                    <asp:ListItem>Reports</asp:ListItem>
                            </asp:DropDownList>
                            <span class="reqLabel">Ext. Data Comm. Recipient:</span>
                            <asp:TextBox Text='<%# Bind("ExtDataCommRecip") %>' runat="server" ID="txtExtDataCommRecip" />

                            <span class="reqLabel">Hospital Location:</span>
                            
                            <asp:ListBox SelectionMode="Multiple" ID="lstHosps" runat="server" ClientIDMode="Static">

                                <asp:ListItem Text="SVUH" Value="1" />
                                <asp:ListItem Text="SVPH" Value="2" />
                                <asp:ListItem Text="SMH" Value="3" />
                                <asp:ListItem Text="SVUH/SVPH" Value="4" />
                                <asp:ListItem Text="SVUH/SVPH/SMH" Value="5" />
                                <asp:ListItem Text="SVPH/SMH" Value="6" />
                                <asp:ListItem Text="SVUH/SMH" Value="7" />
                            </asp:ListBox>

                            <div class="row">
                                <br />
                                <%-- <div class="col-md-9"></div>--%>
                                <div class="col-md-3">
                                    <asp:Button runat="server" Text="Save" CommandName="Insert" ID="AppInsertButton" UseSubmitBehavior="true" CausesValidation="true" />
                                    <button type="button" id="NewAppClosButn">Close</button>
                                    <%--<asp:LinkButton runat="server" Text="Save" CommandName="InsertApp" ID="btnAppSave" OnClick="btnAppSave_Click" CausesValidation="True" />--%>
                                </div>
                            </div>
                        </div>
                        <%--end row--%>
                    </InsertItemTemplate>

                </asp:FormView>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div> <%--end slider div--%>

    <div id="NewCompSlider" class="collapse col-md-12" style="background-color: #f8f8f8; border: solid 1px silver; padding: 15px; border-radius: 3px; margin-bottom: 30px">
        <asp:UpdatePanel runat="server" ID="NewCompUP" UpdateMode="Always" ClientIDMode="Static">
            <ContentTemplate>

                <asp:FormView ID="fvNewComp" ClientIDMode="Static" DataKeyNames="ID" runat="server" DefaultMode="Insert" OnItemCommand="fvNewComp_ItemCommand" OnItemInserting="fvNewComp_ItemInserting" OnItemUpdating="fvNewComp_ItemUpdating" OnModeChanging="fvNewComp_ModeChanging">
                    <EditItemTemplate>
                    </EditItemTemplate>

                    <InsertItemTemplate>
                        <div class="row">
                            <div class="col-md-12">
                             <span class="LabelBoxCenter">Add New Company</span><br />
                             <span class="reqLabel">Company:</span>
                             <asp:TextBox Text='<%# Bind("Company") %>' runat="server" ID="txtNewCompany" />
                            </div>
                            <div class="row">
                                <br />
                                <%-- <div class="col-md-9"></div>--%>
                                <div class="col-md-6">
                                    <asp:Button runat="server" Text="Save" CommandName="Insert" ID="CompInsertButton" UseSubmitBehavior="true" CausesValidation="true" />
                                    <button type="button" id="NewCompClosButn">Close</button>
                                    <%--<asp:LinkButton runat="server" Text="Save" CommandName="InsertApp" ID="btnAppSave" OnClick="btnAppSave_Click" CausesValidation="True" />--%>
                                </div>
                            </div>
                        </div>
                        <%--end row--%>
                    </InsertItemTemplate>

                </asp:FormView>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div> <%--end slider div--%>

    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always" >
        <ContentTemplate>
               
            <asp:HiddenField ClientIDMode="Static" runat="server" ID="sectionOpn" Value="" /> <%--write 'secF' to this to only show data protection section f--%>

            <%--<asp:HiddenField runat="server" ClientIDMode="Static" ID="sectionOpn" Value="" /> --%>

            <%-- Referral fv--%>
            <%--<span class="reqLabelLgInline" data-toggle="collapse" data-target="#fvRVUcolapse">RVU Manager</span><br />--%>
            <%--<h2>Search</h2>--%>
            <div id="mySpinner"></div>

            <div style="margin-top: 90px !important; margin-bottom:25px; font-size: 1.6em; text-align: center">Risk Assessment<span style="color: #9fce2e;">|</span>Editor</div>
            <%--<hr style="border: 0;border-top: 1px solid #ffffff;" />--%>

            <%--<div id="fvRVUcolapse" class="collapse">--%>



            <div class="row">
                <div class="col-md-12" style="border: solid 0px #d4d4d4; padding: 15px; border-radius: 15px; background: #f8f8f8;">
                                <%-- update panel for app name dd as it's refresh is triggered from save event in slider div>fv --%>

                              <%--  <asp:UpdatePanel runat="server" ID="DeptUP" ClientIDMode="Static" UpdateMode="Always">
                                    <ContentTemplate>--%>

                                        <span class="reqLabel" style="font-size: 1.1em; color: #21354d;">Filter By Department:</span>
                                        <asp:DropDownList AppendDataBoundItems="true" AutoPostBack="true" OnInit="ddDeptFilter_Init" OnSelectedIndexChanged="ddDeptFilter_SelectedIndexChanged" runat="server" ID="ddDeptFilter">
                                            <asp:ListItem Text="Please select" Value="" />
                                        </asp:DropDownList>
                                        
                                 <%--   </ContentTemplate>
                                </asp:UpdatePanel>--%>

                            </div>

            </div>

            <div class="row">
                <%--<div class="fvsectn">--%>





                <asp:FormView ID="fvRiskAssess" ClientIDMode="Static" runat="server" DataKeyNames="ID" OnDataBound="fvRiskAssess_DataBound" OnItemCommand="fvRiskAssess_ItemCommand"  OnItemUpdated="fvRiskAssess_ItemUpdated" OnItemInserted="fvRiskAssess_ItemInserted" OnItemUpdating="fvRiskAssess_ItemUpdating" OnItemInserting="fvRiskAssess_ItemInserting" DefaultMode="Edit" OnModeChanging="fvRiskAssess_ModeChanging" OnPageIndexChanging="fvRiskAssess_PageIndexChanging" AllowPaging="true" PagerSettings-Position="TopAndBottom">
                    
                    
                    <EditItemTemplate>

                        <span class="reqLabelLgInline" style="margin-top:10px">Section A: General Information</span><br />
                         <span class="reqLabelSmInlineSub"> Please tick the most relevant answer below for use in Section E</span>
                        <br /> <br />



                        <div class="row">

                            <div class="col-md-12" style="border: solid 0px #d4d4d4; padding: 15px; border-radius: 15px; background: #21354d;">
                                <%-- update panel for app name dd as it's refresh is triggered from save event in slider div>fv --%>

                               <%-- <asp:UpdatePanel runat="server" ID="AppNameUPEdit" ClientIDMode="Static" UpdateMode="Always">
                                    <ContentTemplate> nb: this is commented out because of UP's causing name collisions (this dd will be seen as a duplicate of the one in the insert item template...known issue--%>

                                        <span class="reqLabel" style="font-size: 1.1em; color: #fff;">Item Name:</span>
                                        <asp:DropDownList AppendDataBoundItems="true" OnInit="ddAppNameInsrt_Init" OnSelectedIndexChanged="ddAppNameInsrt_SelectedIndexChanged" runat="server" ID="ddAppNameInsrt" AutoPostBack="true">                                        
                                        </asp:DropDownList>

                                        <%--<button type="button" id="NewAppButn" data-toggle="collapse" data-target="#NewAppSlider">New App</button>--%>
                                        <button type="button" id="NewAppButn">New Item <span style="color: white; font-weight: 600; margin-left: 5px">&orarr;</span></button>

                                    <%--</ContentTemplate>
                                </asp:UpdatePanel>--%>

                            </div>


                        </div>
                                <%-- end row--%>


                        <br />
                        <div class="row" id="NuAppTarg"> <%--dynamically insert new app fv here via jquery above --%>
                            
                        </div>
                        <%--end row bracketing fv and up--%>

                        <br />
                        <div id="boundingbox" style="border: solid 1px #d1d1d1; padding-top: 15px; padding-bottom: 15px; background: #f8f8f8;">
                            <div class="row">

                                <div class="col-md-4">
                                    <span class="reqLabel" style="font-weight: 600">Date Risk Assess.:</span>
                                    <asp:TextBox Text='<%# Bind("DateRiskAssess") %>' runat="server" ID="txtDatRiskAss" />
                                </div>

                                <div class="col-md-4">
                                    <span class="reqLabel" style="font-weight: 600">Name (Carried Out By):</span>
                                    <asp:TextBox Text='<%# Bind("Username") %>' runat="server" ID="txtName" />
                                </div>

                                <div class="col-md-4">
                                    <span class="reqLabel" style="font-weight: 600;">Primary Department:</span>
                                    <asp:DropDownList AppendDataBoundItems="true" OnInit="ddDept_Init" runat="server" ID="ddDept" Style="max-width: 150px;">
                                        <asp:ListItem Text="Please select" Value="" />
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="row" style="padding: 5px; border-radius: 3px; margin-bottom: 10px">

                            <div class="col-md-4">
                                <span class="reqLabel">Name Of App.Owner:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("AppOwnerName") %>' runat="server" ID="chkAppOwnrNam" />--%>
                                <asp:TextBox Text='<%# Bind("AppOwnerName") %>' runat="server" ID="txtAppOwnerName" /><br />
                            </div>

                                <div class="col-md-8">
                                    <span class="reqLabel">Description:</span>
                                    <asp:TextBox Text='<%# Bind("Notes") %>' runat="server" ID="txtNotes" TextMode="MultiLine" /><br />
                                </div>
                            </div>

                        </div>

                        <br />
<div id="secA" class="collapse in">

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">


                            <div class="col-md-4">
                                <span class="reqLabel">Age of Hardware (Yrs):</span>
                                <asp:TextBox Text='<%# Bind("AgeHardware") %>' runat="server" ID="txtHWAge" />
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel"  title="Has there been many failures \ replacements over what would have been expected? Is the hardware clean or is it covered in dust? Is there an unacceptable number of repairs required to keep it working?">H/W Condition:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareCondition") %>' runat="server" ID="ddHWCond">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="If appropriate, Is the location that the hardware physically located secure from unauthorised access? Is it in a temperature controlled area is the power covered by an uninterruptable power supply (UPS)? Is it protected from being inadvertently being powered off or being physically damaged. Is it under a desk?">H/W Environ.:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareEnviron") %>' runat="server" ID="ddHWEnviron">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <span class="reqLabel" title="If appropriate, Is the hardware capable of running when some components have failed? Is there a second system which can be switched over to in the event of a failure?">H/W Resil./Redunancy:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareResilRedun") %>' runat="server" ID="ddHWResilRedun">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="Are all software security patches applied in a timely manner?">Patches/Updates Applied:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareUpdated") %>' runat="server" ID="ddHWUpdated">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="Is Anti-Virus software functional and up to date?">A/V Current/Functional:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareAV") %>' runat="server" ID="ddHWAV">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                    <asp:ListItem>N/A</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>



<%--                        <div class="row">
                            <div class="col-md-12">
                                <span class="reqLabel">Description:</span>
                                <asp:TextBox Text='<%# Bind("ActivityName") %>' runat="server" ID="txtDesc" TextMode="MultiLine" /><br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="reqLabel">Start Date:</span>
                                <asp:TextBox Text='<%# Bind("StartDate") %>' runat="server" ID="txtStartDt" required="required" /><br />
                            </div>
                            <div class="col-md-6">
                                <span class="reqLabel">End Date:</span>
                                <asp:TextBox Text='<%# Bind("EndDate") %>' runat="server" ID="txtEndDt" required="required" /><br />
                            </div>
                        </div>--%>
                        <br />

</div><%--end secA collapsible--%>  
<div id="secB" class="collapse in">
                        <span class="reqLabelLgInline">Section B: Protecting Information - Backups </span><br />
                        <span class="reqLabelSmInlineSub"> In order to complete this section you may need to liaise with the ICT Department</span><br />
                       <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Is the backup media (Tape \ Disk) physically located secure from unauthorised access? Is a copy stored in a remote location to the live data? Is a copy stored in a fireproof location?">Backup Media Secure:</span>
                                <%--<asp:CheckBox  Checked='<%# bool.Parse(Eval("BackupMediaSecure").ToString()) %>' runat="server" ID="chkBMS" />--%>
                                <%--<asp:CheckBox Checked='<%# Bind("BackupMediaSecure") %>' runat="server" ID="chkBMS" /> --%> 
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupMediaSecure") %>' runat="server" ID="ddBMS">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupMediaSecureComment") %>' TextMode="MultiLine" runat="server" ID="txtBMSCmt" /><br />
                            </div>
                        </div>

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Is all the data of the application backed up, are there databases, or file locations missing from the backup being performed? Is everything being backed up to recover the system in the event of a failure?">Backups Cover Locations:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("BackupReqLocation") %>' runat="server" ID="chkBackLoc" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupReqLocation") %>' runat="server" ID="ddBackLoc">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupReqLocationComment") %>' TextMode="MultiLine" runat="server" ID="txtBackLocCmnt" /><br />
                            </div>
                        </div>
                       
<%--                         <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel">Backups Ensure RTO/RPO Objectives.:</span>

                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupRTORPO") %>' runat="server" ID="ddBackRTORPO">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>

                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupRTORPOComment") %>' TextMode="MultiLine" runat="server" ID="txtBackRTORPOCmnt" /><br />
                            </div>
                        </div>--%>

                        <br />
                        
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Recovery Time Objective (RTO) is the targeted duration of time within which the application must be restored after failure to avoid unacceptable consequences.">RTO Defined (Time):</span>
                            </div>
                            <div class="col-md-9">
                                
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupRTODefined") %>' runat="server" ID="ddBackRTOTime">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>15 mins / Continuous</asp:ListItem>
                                    <asp:ListItem>6 Hours</asp:ListItem>
                                    <asp:ListItem>1 Day</asp:ListItem>
                                    <asp:ListItem>1 Week</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                            </div>
                        </div>

                        <br />
                        
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Recovery point objective (RPO) is the maximum targeted period in which data might be lost after failure before unacceptable consequences occur.">RPO Defined (Time):</span>
                            </div>
                            <div class="col-md-9">
                                
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupRPODefined") %>' runat="server" ID="ddBackRPOTime">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>15 mins / Continuous</asp:ListItem>
                                    <asp:ListItem>6 Hours</asp:ListItem>
                                    <asp:ListItem>1 Day</asp:ListItem>
                                    <asp:ListItem>1 Week</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="The data in backups are solely used for the purpose of recovering failed systems. Backups must not be used as a store for old or inactive data which has been removed from a live system (Archive).">Backup Archives Merged:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("BackupsArchiveNotMerged") %>' runat="server" ID="chkBackArcNotMerged" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupsArchiveNotMerged") %>' runat="server" ID="ddBackArcNotMerged">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupsArchiveNotMergedComment") %>' TextMode="MultiLine" runat="server" ID="txtBackArcNotMergedCmnt" /><br />
                            </div>
                        </div>

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Has the backup been tested to ensure that it works as required meeting the stated RTO RPO?">Recoverability Test Completed:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("RecoverabilityTestCompleted") %>' runat="server" ID="chkRecTestComplete" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("RecoverabilityTestCompleted") %>' runat="server" ID="ddRecTestComplete">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("RecoverabilityTestCompletedComment") %>' TextMode="MultiLine" runat="server" ID="txtRecTestCompleteCmnt" /><br />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Are there adequate support arrangements in place including service level agreements (SLA)">Support & SLA's in place (H/W, OS & App.):</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("SLASupport") %>' runat="server" ID="chkSLASupt" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("SLASupport") %>' runat="server" ID="ddSLASupt">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("SLASupportComment") %>' TextMode="MultiLine" runat="server" ID="txtSLASuptCmnt" /><br />
                            </div>
                        </div>

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="If applicable are KPI’s in any SLAs reviewed?">KPI's Reviewed:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("SLAKPIReviewed") %>' runat="server" ID="chkKPIRevd" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("SLAKPIReviewed") %>' runat="server" ID="ddKPIRevd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("SLAKPIReviewedComment") %>' TextMode="MultiLine" runat="server" ID="txtKPIRevdCmnt" /><br />
                            </div>
                        </div>

                     

</div><%--end secB collapsible--%>  
<div id="secC" class="collapse in">
                        <span class="reqLabelLgInline">Section C: Documentation </span><br />
                        <span class="reqLabelSmInlineSub"> Have the following policies / documents been adhered to and signed copies retained:</span>
                        <br />

                        <div class="row">
                            <div class="col-md-6">
                                <span class="reqLabel" title="Please refer to QPulse for the policy">Core App. Access Policy:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CoreAppAccessPolicy") %>' runat="server" ID="chkCoreAppAccPol" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CoreAppAccessPolicy") %>' runat="server" ID="ddCoreAppAccPol">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <span class="reqLabel" title="Please refer to QPulse for the policy">Core App. Change Mgmt.:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CoreAppChangeMgt") %>' runat="server" ID="chkCoreAppChngeMgt" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CoreAppChangeMgt") %>' runat="server" ID="ddCoreAppChngeMgt">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                           <%-- <div class="col-md-3">
                                <span class="reqLabel">Confidentiality Supplier sig.:</span>
                                
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ConfidentSupplierSigned") %>' runat="server" ID="ddConfSuppSgnd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        </div>
                        <br />
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-6">
                                <span class="reqLabel" title="Please refer to QPulse for the policy">3rd Party Remote Access Policy sig.:</span>
                               <%-- <asp:CheckBox  Checked='<%# Bind("ThirdPartyRemAccSigned") %>' runat="server" ID="chkThrdPartyRemAccSgnd" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ThirdPartyRemAccSigned") %>' runat="server" ID="ddThrdPartyRemAccSgnd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                    <asp:ListItem>N/A</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <%--<span class="reqLabel">3rd Party Remote Access Policy N/A:</span>
                                
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ThirdPartyRemAccSignedNA") %>' runat="server" ID="ddThrdPartyRemAccSgndNA">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>--%>
                            </div>
                        </div>
</div><%--end secC collapsible--%>  
<div id="secD" class="collapse in">                                               
                        <span class="reqLabelLgInline">Section D: Data Protection Compliance </span>
                        <br /><br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Why do we need performing this task?">Purpose of Process:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("ProcessPurpose") %>' TextMode="MultiLine" runat="server" ID="txtProcPurp" /><br />
                            </div>
                        </div>
                        <br />

                         <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Does the data get transferred outside of the EEA? (EU + approved countries)">Process involve Cross Border transfers?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("ProcessCrossBorderTrans") %>' runat="server" ID="chkProcXBordTrans" />--%>                            
                            </div>
                             <div class="col-md-9">
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ProcessCrossBorderTrans") %>' runat="server" ID="ddProcXBordTrans">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                             </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="What method of is used to transfer the information?">If so, how is this done?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderHowDone") %>' TextMode="MultiLine" runat="server" ID="txtProcXBordTransHow" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="List the data items contained in the processing">What data is involved?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransData") %>' TextMode="MultiLine" runat="server" ID="txtXBordTransData" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="How long will you hold this information?">How long will data be retained for?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataRetainLength") %>' TextMode="MultiLine" runat="server" ID="txtXBordRetLen" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Physical location of the data">Where is the data stored?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataStorage") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataStore" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Data Processor">By Whom?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataStorageBy") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataStoreBy" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="(Clinical, Employment, Educational etc)">What type of data is this?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataType") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataType" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Does a 3rd party handle processing">Is processing outsourced?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataOutsourced") %>' runat="server" ID="chkXBordDataOutsrcd" />--%>
                            </div>
                            <div class="col-md-9">
                            <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataOutsourced") %>' runat="server" ID="ddXBordDataOutsrcd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="List all (sub)Processors">Please list processers and sub-processors:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataProcessorList") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataProcList" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Should this be included as part of a Subject Access Request when releasing information to the patient">Is this exempt from SAR?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataExemptSAR") %>' runat="server" ID="chkXBordDataExemptSAR" />--%>
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataExemptSAR") %>' runat="server" ID="ddXBordDataExemptSAR">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Why not?">If exempt, why?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataExemptSARReason") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataExemptSARRson" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="What security measures do you have in place?">What safeguards are in place to secure the data?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataSafeguards") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataSafe" /><br />
                            </div>
                        </div>
                        
                        <br /><br />

                        <div class="row">
                            <div class="col-md-4">
                                <span class="reqLabel" title="Can we delete all information about a particular patient, if requested?">Right to be forgotten implemented on dataset?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataRightForgotten") %>' runat="server" ID="chkXBordDataRightForget" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataRightForgotten") %>' runat="server" ID="ddXBordDataRightForget">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        
                            <div class="col-md-4">
                                <span class="reqLabel" title="Can we limit the sharing of this data?">Right to prevent processing implemented?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataRightPreventProcess") %>' runat="server" ID="chkXBordDataRightPreventProc" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataRightPreventProcess") %>' runat="server" ID="ddXBordDataRightPreventProc">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                             <div class="col-md-4">
                                <span class="reqLabel" title="Does the system capture a log of anybody who views or changes the data?">Audit log for who accessed the patient data?:</span>
                             <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransAuditLogPxDataAccess") %>' runat="server" ID="chkXBordDataAuditLogPxData" />--%>
                                 <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransAuditLogPxDataAccess") %>' runat="server" ID="ddXBordDataAuditLogPxData">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br /><br />

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            
                            <div class="col-md-3">
                                <span class="reqLabel">Does the processing of this data comply with the 6 following principles listed?:</span>
                            </div>

                            <div class="col-md-8" style="font-weight:600;margin-left:15px">
                              <span class="reqLabel" title="Personal data must be processed lawfully, fairly, and transparently">Lawfulness: </span> <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyLawfulness") %>' runat="server" ID="chkXBordComplyLaw" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyLawfulness") %>' runat="server" ID="ddXBordComplyLaw">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Data Collected for specified, explicit, and legitimate purposes">Purpose limitations: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyPurposeLimit") %>' runat="server" ID="chkXBordComplyPurpLmt" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyPurposeLimit") %>' runat="server" ID="ddXBordComplyPurpLmt">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Only collect the personal data that is necessary for the purpose of the business function">Data minimization: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyDataMin") %>' runat="server" ID="chkXBordComplyDataMin" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyDataMin") %>' runat="server" ID="ddXBordComplyDataMin">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Personal data must be kept accurate and current">Accuracy: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyIntegAccuracy") %>' runat="server" ID="chkXBordComplyAccur" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyIntegAccuracy") %>' runat="server" ID="ddXBordComplyAccur">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Do you retain the data longer than you require">Storage limitations: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyStorageLimit") %>' runat="server" ID="chkXBordComplyStrgLim" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyStorageLimit") %>' runat="server" ID="ddXBordComplyStrgLim">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="The confidentiality and integrity of the personal data must always be maintained">Integrity and confidentiality: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyIntegConfident") %>' runat="server" ID="chkXBordComplyIntConf" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyIntegConfident") %>' runat="server" ID="ddXBordComplyIntConf">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
 
                            </div>
                        </div>

                        <br /><br />

                        <div class="row">
                            <div class="col-md-6">
                                <span class="reqLabel">Is this classified as sensitive data?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataClassifiedSensitive") %>' runat="server" ID="chkXBordClassSens" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataClassifiedSensitive") %>' runat="server" ID="ddXBordClassSens">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        
                            <div class="col-md-6">
                                <span class="reqLabel">Data Processor addendum in place?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataProcessorAddendum") %>' runat="server" ID="chkXBordProcAddend" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataProcessorAddendum") %>' runat="server" ID="ddXBordProcAddend">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br />



</div><%--end secD collapsible--%> 
<div id="secE" class="collapse in">
                        <span class="reqLabelLgInline">Section E: Calculating ICT Risks </span>
                        <br />

                        <span class="reqLabelSmInlineSub"> In order to calculate the risk rating (value), consider your answers for Section A and B and then review each of the five available options for Part 1 and Part 2 below, selecting the category for each that is most relevant. Note the score you have selected in each instance. These scores may then be compared against the table below (Part 3), in order to establish an accurate risk rate, e.g. Green, Amber or Red. An example is as follows: Likelihood score of 3 x Impact score of 4 = 12/25, Amber (48%).</span><br />

                        <div class="row">
                            <div class="col-md-9">
                                <img class="img-responsive" src="Content/Pt1Screenie.png" />
                            </div>
                            <div class="col-md-3">
                                <span class="reqLabel">(Pt.1) Likelihood of Risk:</span>
                               <asp:TextBox Text='<%# Bind("Part1RiskRating") %>' runat="server" ID="txtPt1RiskRat" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-9">
                                <img class="img-responsive" src="Content/Pt2Screenie.png" />
                            </div>
                            <div class="col-md-3">
                                <span class="reqLabel">(Pt.2) Impact of Risk:</span>
                               <asp:TextBox Text='<%# Bind("Part2RiskRating") %>' runat="server" ID="txtPt2RiskRat" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-9">
                                <img class="img-responsive" src="Content/Pt3Screenie.png" />
                            </div>
                            <div class="col-md-3">
                                <span class="reqLabel">(Pt.3) Risk Rating (Overall):</span>
                                <asp:TextBox Text='<%# Bind("OverallRiskRating") %>' runat="server" ID="txtOverallRiskRat" /><br />
                            </div>
                        </div>
                        <br /><br />
                        
                        <span class="reqLabelSmInlineSub"> Where it is not possible to address the items identified above, a risk must be raised in the departmental risk register and managed according to departmental risk management processes.</span><br />
                       
                        <br />

                        <div class="row">
                            <div class="col-md-12">
                                 <%--<asp:Button runat="server" Text="New" CommandName="New" ID="NewEditButton" CausesValidation="true" />&nbsp;--%><asp:Button runat="server" Text="Update" CommandName="Update" ID="UpdateButton" CausesValidation="True" />&nbsp;<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="UpdateCancelButton" CausesValidation="False" />
                            </div>
                        </div>
</div><%--end secE collapsible--%>  
 

                    </EditItemTemplate>

                    <InsertItemTemplate>

                        <span class="reqLabelLgInline" style="margin-top:10px">Section A: General Information</span><br />
                         <span class="reqLabelSmInlineSub"> Please tick the most relevant answer below for use in Section E</span>
                        <br /> <br />


                       
                        <div class="row">


                            <div class="col-md-12" style="border: solid 0px #d4d4d4; padding: 15px; border-radius: 15px; background: #21354d;">

                                <%-- update panel for app name dd as it's refresh is triggered from save event in slider div>fv --%>
                                <asp:UpdatePanel runat="server" ID="AppNameUPInsrt" ClientIDMode="Static" UpdateMode="Always">
                                    <ContentTemplate>
                                        <span class="reqLabel" style="font-size: 1.1em; color: #fff;">Item Name:</span>
                                        <asp:DropDownList AppendDataBoundItems="true" OnInit="ddAppNameInsrt_Init" OnSelectedIndexChanged="ddAppNameInsrt_SelectedIndexChanged" runat="server" ID="ddAppNameInsrt" AutoPostBack="true">
                                        </asp:DropDownList>
                                        <%--<button type="button" id="NewAppButn" data-toggle="collapse" data-target="#NewAppSlider">New App</button>--%>
                                        <button type="button" id="NewAppButn">New Item <span style="color: white; font-weight: 600; margin-left: 5px">&orarr;</span></button>
                                    </ContentTemplate>
                                </asp:UpdatePanel>


                            </div>


                        </div>
                        <%-- end row--%>



                        <br />
                        <div class="row" id="NuAppTarg"> <%--dynamically insert new app fv here via jquery above --%>
                            
                        </div>
                        <%--end row bracketing fv and up--%>

                        <br />
                        <div id="boundingbox" style="border: solid 1px #d1d1d1; padding-top: 15px; padding-bottom: 15px; background: #f8f8f8;">
                            <div class="row">

                                <div class="col-md-4">
                                    <span class="reqLabel" style="font-weight: 600">Date Risk Assess.:</span>
                                    <asp:TextBox Text='<%# Bind("DateRiskAssess") %>' runat="server" ID="txtDatRiskAss" />
                                </div>

                                <div class="col-md-4">
                                    <span class="reqLabel" style="font-weight: 600">Name (Carried Out By):</span>
                                    <asp:TextBox Text='<%# Bind("Username") %>' runat="server" ID="txtName" />
                                </div>

                                <div class="col-md-4">
                                    <span class="reqLabel" style="font-weight: 600;">Primary Department:</span>
                                    <asp:DropDownList AppendDataBoundItems="true" OnInit="ddDept_Init" runat="server" ID="ddDept" Style="max-width: 150px;">
                                        <asp:ListItem Text="Please select" Value="" />
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="row" style="padding: 5px; border-radius: 3px; margin-bottom: 10px">

                             <div class="col-md-4">
                                <span class="reqLabel">Name Of App.Owner:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("AppOwnerName") %>' runat="server" ID="chkAppOwnrNam" />--%>
                                <asp:TextBox Text='<%# Bind("AppOwnerName") %>' runat="server" ID="txtAppOwnerName" /><br />
                            </div>

                                <div class="col-md-8">
                                    <span class="reqLabel">Description:</span>
                                    <asp:TextBox Text='<%# Bind("Notes") %>' runat="server" ID="txtNotes" TextMode="MultiLine" /><br />
                                </div>
                            </div>

                        </div>

                        <br />
<div id="secA" class="collapse in">
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">

                            <div class="col-md-4">
                                <span class="reqLabel">Age of Hardware (Yrs):</span>
                                <asp:TextBox Text='<%# Bind("AgeHardware") %>' runat="server" ID="txtHWAge" />
                            </div>

                            <div class="col-md-4">  
                                <span class="reqLabel" title="Has there been many failures \ replacements over what would have been expected? Is the hardware clean or is it covered in dust? Is there an unacceptable number of repairs required to keep it working?">H/W Condition:</span>
                                   <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareCondition") %>' runat="server" ID="ddHWCond">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="If appropriate, Is the location that the hardware physically located secure from unauthorised access? Is it in a temperature controlled area is the power covered by an uninterruptable power supply (UPS)? Is it protected from being inadvertently being powered off or being physically damaged. Is it under a desk?">H/W Environ.:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareEnviron") %>' runat="server" ID="ddHWEnviron">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <span class="reqLabel" title="If appropriate, Is the hardware capable of running when some components have failed? Is there a second system which can be switched over to in the event of a failure">H/W Resil./Redunancy:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareResilRedun") %>' runat="server" ID="ddHWResilRedun">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="Are all software security patches applied in a timely manner?">Patches/Updates Applied:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareUpdated") %>' runat="server" ID="ddHWUpdated">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="Is Anti-Virus software functional and up to date?">A/V Current/Functional:</span>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("HardwareAV") %>' runat="server" ID="ddHWAV">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Excellent</asp:ListItem>
                                    <asp:ListItem>Very Good</asp:ListItem>
                                    <asp:ListItem>Good</asp:ListItem>
                                    <asp:ListItem>Poor</asp:ListItem>
                                    <asp:ListItem>Unnacceptable</asp:ListItem>
                                    <asp:ListItem>N/A</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                        </div>
                        

<%--                        <div class="row">
                            <div class="col-md-12">
                                <span class="reqLabel">Description:</span>
                                <asp:TextBox Text='<%# Bind("ActivityName") %>' runat="server" ID="txtDesc" TextMode="MultiLine" /><br />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <span class="reqLabel">Start Date:</span>
                                <asp:TextBox Text='<%# Bind("StartDate") %>' runat="server" ID="txtStartDt" required="required" /><br />
                            </div>
                            <div class="col-md-6">
                                <span class="reqLabel">End Date:</span>
                                <asp:TextBox Text='<%# Bind("EndDate") %>' runat="server" ID="txtEndDt" required="required" /><br />
                            </div>
                        </div>--%>
                        <br />
</div> <%--end secA collapsible--%>
<div id="secB" class="collapse in">                    
                        <span class="reqLabelLgInline">Section B: Protecting Information - Backups </span><br />
                        <span class="reqLabelSmInlineSub"> In order to complete this section you may need to liaise with the ICT Department</span><br />
                       <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Is the backup media (Tape \ Disk) physically located secure from unauthorised access? Is a copy stored in a remote location to the live data? Is a copy stored in a fireproof location?">Backup Media Secure:</span>
                                <%--<asp:CheckBox  Checked='<%# bool.Parse(Eval("BackupMediaSecure").ToString()) %>' runat="server" ID="chkBMS" />--%>
                                <%--<asp:CheckBox  Checked='<%# Bind("BackupMediaSecure") %>' runat="server" ID="chkBMS" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupMediaSecure") %>' runat="server" ID="ddBMS">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupMediaSecureComment") %>' TextMode="MultiLine" runat="server" ID="txtBMSCmt" /><br />
                            </div>
                        </div>

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Is all the data of the application backed up, are there databases, or file locations missing from the backup being performed? Is everything being backed up to recover the system in the event of a failure?">Backups Cover Locations:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("BackupReqLocation") %>' runat="server" ID="chkBackLoc" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupReqLocation") %>' runat="server" ID="ddBackLoc">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupReqLocationComment") %>' TextMode="MultiLine" runat="server" ID="txtBackLocCmnt" /><br />
                            </div>
                        </div>
                       
<%--                         <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel">Backups Ensure RTO/RPO Objectives.:</span>
                               
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("xBackupRTORPOxx") %>' runat="server" ID="ddBackRTORPO">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupRTORPOComment") %>' TextMode="MultiLine" runat="server" ID="txtBackRTORPOCmnt" /><br />
                            </div>
                        </div>--%>

                        <br />
                        
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Recovery Time Objective (RTO) is the targeted duration of time within which the application must be restored after failure to avoid unacceptable consequences.">RTO Defined (Time):</span>
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupRTODefined") %>' runat="server" ID="ddBackRTOTime">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>15 mins / Continuous</asp:ListItem>
                                    <asp:ListItem>6 Hours</asp:ListItem>
                                    <asp:ListItem>1 Day</asp:ListItem>
                                    <asp:ListItem>1 Week</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                            </div>
                        </div>

                        <br />
                        
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Recovery point objective (RPO) is the maximum targeted period in which data might be lost after failure before unacceptable consequences occur.">RPO Defined (Time):</span>
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupRPODefined") %>' runat="server" ID="ddBackRPOTime">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>15 mins / Continuous</asp:ListItem>
                                    <asp:ListItem>6 Hours</asp:ListItem>
                                    <asp:ListItem>1 Day</asp:ListItem>
                                    <asp:ListItem>1 Week</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="The data in backups are solely used for the purpose of recovering failed systems. Backups must not be used as a store for old or inactive data which has been removed from a live system (Archive).">Backup Archives Merged:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("BackupsArchiveNotMerged") %>' runat="server" ID="chkBackArcNotMerged" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("BackupsArchiveNotMerged") %>' runat="server" ID="ddBackArcNotMerged">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("BackupsArchiveNotMergedComment") %>' TextMode="MultiLine" runat="server" ID="txtBackArcNotMergedCmnt" /><br />
                            </div>
                        </div>

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Has the backup been tested to ensure that it works as required meeting the stated RTO RPO?">Recoverability Test Completed:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("RecoverabilityTestCompleted") %>' runat="server" ID="chkRecTestComplete" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("RecoverabilityTestCompleted") %>' runat="server" ID="ddRecTestComplete">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("RecoverabilityTestCompletedComment") %>' TextMode="MultiLine" runat="server" ID="txtRecTestCompleteCmnt" /><br />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Are there adequate support arrangements in place including service level agreements (SLA)">Support & SLA's in place (H/W, OS & App.):</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("SLASupport") %>' runat="server" ID="chkSLASupt" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("SLASupport") %>' runat="server" ID="ddSLASupt">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("SLASupportComment") %>' TextMode="MultiLine" runat="server" ID="txtSLASuptCmnt" /><br />
                            </div>
                        </div>

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-3">
                                <span class="reqLabel" title="If applicable are KPI’s in any SLAs reviewed?">KPI's Reviewed:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("SLAKPIReviewed") %>' runat="server" ID="chkKPIRevd" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("SLAKPIReviewed") %>' runat="server" ID="ddKPIRevd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-9">
                                <span class="reqLabel">Comment:</span>
                                <asp:TextBox Text='<%# Bind("SLAKPIReviewedComment") %>' TextMode="MultiLine" runat="server" ID="txtKPIRevdCmnt" /><br />
                            </div>
                        </div>

</div> <%--end secB collapsible--%>                    
<div id="secC" class="collapse in"> 

                        <span class="reqLabelLgInline">Section C: Documentation </span><br />
                        <span class="reqLabelSmInlineSub"> Have the following policies / documents been adhered to and signed copies retained:</span>
                        <br />

                        <div class="row">
                            <div class="col-md-6">
                                <span class="reqLabel" title="Please refer to QPulse for the policy">Core App. Access Policy:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CoreAppAccessPolicy") %>' runat="server" ID="chkCoreAppAccPol" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CoreAppAccessPolicy") %>' runat="server" ID="ddCoreAppAccPol">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <span class="reqLabel" title="Please refer to QPulse for the policy">Core App. Change Mgmt.:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CoreAppChangeMgt") %>' runat="server" ID="chkCoreAppChngeMgt" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CoreAppChangeMgt") %>' runat="server" ID="ddCoreAppChngeMgt">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                          <%--  <div class="col-md-3">
                                <span class="reqLabel">Confidentiality Supplier sig.:</span>
                                
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ConfidentSupplierSigned") %>' runat="server" ID="ddConfSuppSgnd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>--%>
                        </div>
                        <br />
                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            <div class="col-md-6">
                                <span class="reqLabel" title="Please refer to QPulse for the policy">3rd Party Remote Access Policy sig.:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("ThirdPartyRemAccSigned") %>' runat="server" ID="chkThrdPartyRemAccSgnd" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ThirdPartyRemAccSigned") %>' runat="server" ID="ddThrdPartyRemAccSgnd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                    <asp:ListItem>N/A</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-6">
                                <%--<span class="reqLabel">3rd Party Remote Access Policy N/A:</span>
                               
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ThirdPartyRemAccSignedNA") %>' runat="server" ID="ddThrdPartyRemAccSgndNA">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>--%>
                            </div>
                        </div>

</div> <%--end secC collapsible--%>   
<div id="secD" class="collapse in">

                        <span class="reqLabelLgInline">Section D: Data Protection Compliance </span>
                        <br /><br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Why do we need performing this task?">Purpose of Process:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("ProcessPurpose") %>' TextMode="MultiLine" runat="server" ID="txtProcPurp" /><br />
                            </div>
                        </div>
                        <br />

                         <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Does the data get transferred outside of the EEA? (EU + approved countries)">Process involve Cross Border transfers?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("ProcessCrossBorderTrans") %>' runat="server" ID="chkProcXBordTrans" />--%>
                            </div>
                             <div class="col-md-9">
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("ProcessCrossBorderTrans") %>' runat="server" ID="ddProcXBordTrans">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                             </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="What method of is used to transfer the information?">If so, how is this done?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderHowDone") %>' TextMode="MultiLine" runat="server" ID="txtProcXBordTransHow" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="List the data items contained in the processing">What data is involved?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransData") %>' TextMode="MultiLine" runat="server" ID="txtXBordTransData" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="How long will you hold this information?">How long will data be retained for?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataRetainLength") %>' TextMode="MultiLine" runat="server" ID="txtXBordRetLen" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Physical location of the data">Where is the data stored?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataStorage") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataStore" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Data Processor">By Whom?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataStorageBy") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataStoreBy" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="(Clinical, Employment, Educational etc)">What type of data is this?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataType") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataType" /><br />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Does a 3rd party handle processing">Is processing outsourced?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataOutsourced") %>' runat="server" ID="chkXBordDataOutsrcd" />--%>
                                
                            </div>
                            <div class="col-md-9">
                            <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataOutsourced") %>' runat="server" ID="ddXBordDataOutsrcd">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="List all (sub)Processors">Please list processers and sub-processors:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataProcessorList") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataProcList" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Should this be included as part of a Subject Access Request when releasing information to the patient">Is this exempt from SAR?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataExemptSAR") %>' runat="server" ID="chkXBordDataExemptSAR" />--%>
                            </div>
                            <div class="col-md-9">
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataExemptSAR") %>' runat="server" ID="ddXBordDataExemptSAR">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="Why not?">If exempt, why?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataExemptSARReason") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataExemptSARRson" /><br />
                            </div>
                        </div>
                        <br />

                        <div class="row">
                            <div class="col-md-3">
                                <span class="reqLabel" title="What security measures do you have in place?">What safeguards are in place to secure the data?:</span>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox Text='<%# Bind("CrossBorderTransDataSafeguards") %>' TextMode="MultiLine" runat="server" ID="txtXBordDataSafe" /><br />
                            </div>
                        </div>
                        
                        <br /><br />

                        <div class="row">
                            <div class="col-md-4">
                                <span class="reqLabel" title="Can we delete all information about a particular patient, if requested?">Right to be forgotten implemented on dataset?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataRightForgotten") %>' runat="server" ID="chkXBordDataRightForget" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataRightForgotten") %>' runat="server" ID="ddXBordDataRightForget">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        
                            <div class="col-md-4">
                                <span class="reqLabel" title="Can we limit the sharing of this data?">Right to prevent processing implemented?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataRightPreventProcess") %>' runat="server" ID="chkXBordDataRightPreventProc" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataRightPreventProcess") %>' runat="server" ID="ddXBordDataRightPreventProc">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>

                            <div class="col-md-4">
                                <span class="reqLabel" title="Does the system capture a log of anybody who views or changes the data?">Audit log for who accessed the patient data?:</span>
                             <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransAuditLogPxDataAccess") %>' runat="server" ID="chkXBordDataAuditLogPxData" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransAuditLogPxDataAccess") %>' runat="server" ID="ddXBordDataAuditLogPxData">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br /><br />

                        <div class="row" style="background:#f4f4f4;padding:5px;border-radius:3px;margin-bottom:10px">
                            
                            <div class="col-md-3">
                                <span class="reqLabel">Does the processing of this data comply with the 6 following principles listed?:</span>
                            </div>

                            <div class="col-md-8" style="font-weight:600;margin-left:15px">
                              <span class="reqLabel" title="Personal data must be processed lawfully, fairly, and transparently">Lawfulness: </span> <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyLawfulness") %>' runat="server" ID="chkXBordComplyLaw" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyLawfulness") %>' runat="server" ID="ddXBordComplyLaw">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Data Collected for specified, explicit, and legitimate purposes">Purpose limitations: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyPurposeLimit") %>' runat="server" ID="chkXBordComplyPurpLmt" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyPurposeLimit") %>' runat="server" ID="ddXBordComplyPurpLmt">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Only collect the personal data that is necessary for the purpose of the business function">Data minimization: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyDataMin") %>' runat="server" ID="chkXBordComplyDataMin" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyDataMin") %>' runat="server" ID="ddXBordComplyDataMin">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Personal data must be kept accurate and current">Accuracy: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyIntegAccuracy") %>' runat="server" ID="chkXBordComplyAccur" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyIntegAccuracy") %>' runat="server" ID="ddXBordComplyAccur">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="Do you retain the data longer than you require">Storage limitations: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyStorageLimit") %>' runat="server" ID="chkXBordComplyStrgLim" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyStorageLimit") %>' runat="server" ID="ddXBordComplyStrgLim">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                              <br />
                                <span class="reqLabel" title="The confidentiality and integrity of the personal data must always be maintained">Integrity and confidentiality: </span>  <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataComplyIntegConfident") %>' runat="server" ID="chkXBordComplyIntConf" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataComplyIntegConfident") %>' runat="server" ID="ddXBordComplyIntConf">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
 
                            </div>
                        </div>

                        <br /><br />

                        <div class="row">
                            <div class="col-md-6">
                                <span class="reqLabel">Is this classified as sensitive data?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataClassifiedSensitive") %>' runat="server" ID="chkXBordClassSens" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataClassifiedSensitive") %>' runat="server" ID="ddXBordClassSens">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        
                            <div class="col-md-6">
                                <span class="reqLabel">Data Processor addendum in place?:</span>
                                <%--<asp:CheckBox  Checked='<%# Bind("CrossBorderTransDataProcessorAddendum") %>' runat="server" ID="chkXBordProcAddend" />--%>
                                <asp:DropDownList AppendDataBoundItems="true" Text='<%# Bind("CrossBorderTransDataProcessorAddendum") %>' runat="server" ID="ddXBordProcAddend">
                                    <asp:ListItem Text="Please select" Value="" />
                                    <asp:ListItem>Yes</asp:ListItem>
                                    <asp:ListItem>No</asp:ListItem>
                                    <asp:ListItem>Unknown</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <br />



</div> <%--end secD collapsible--%>
<div id="secE" class="collapse in">
                            <span class="reqLabelLgInline">Section E: Calculating ICT Risks </span>
                            <br />

                            <span class="reqLabelSmInlineSub">In order to calculate the risk rating (value), consider your answers for Section A and B and then review each of the five available options for Part 1 and Part 2 below, selecting the category for each that is most relevant. Note the score you have selected in each instance. These scores may then be compared against the table below (Part 3), in order to establish an accurate risk rate, e.g. Green, Amber or Red. An example is as follows: Likelihood score of 3 x Impact score of 4 = 12/25, Amber (48%).</span><br />

                            <div class="row">
                                <div class="col-md-9">
                                    <img class="img-responsive" src="Content/Pt1Screenie.png" />
                                </div>
                                <div class="col-md-3">
                                    <span class="reqLabel">(Pt.1) Likelihood of Risk:</span>
                                    <asp:TextBox Text='<%# Bind("Part1RiskRating") %>' runat="server" ID="txtPt1RiskRat" /><br />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-md-9">
                                    <img class="img-responsive" src="Content/Pt2Screenie.png" />
                                </div>
                                <div class="col-md-3">
                                    <span class="reqLabel">(Pt.2) Impact of Risk:</span>
                                    <asp:TextBox Text='<%# Bind("Part2RiskRating") %>' runat="server" ID="txtPt2RiskRat" /><br />
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <div class="col-md-9">
                                    <img class="img-responsive" src="Content/Pt3Screenie.png" />
                                </div>
                                <div class="col-md-3">
                                    <span class="reqLabel">(Pt.3) Risk Rating (Overall):</span>
                                    <asp:TextBox Text='<%# Bind("OverallRiskRating") %>' runat="server" ID="txtOverallRiskRat" /><br />
                                </div>
                            </div>
                            <br />
                            <br />

                            <span class="reqLabelSmInlineSub">Where it is not possible to address the items identified above, a risk must be raised in the departmental risk register and managed according to departmental risk management processes.</span><br />

                            <div class="row">
                                <div class="col-md-12">
                                    <asp:Button runat="server" Text="Save" CommandName="Insert" ID="InsertButton" CausesValidation="True" />&nbsp;<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="InsertCancelButton" CausesValidation="False" />
                                </div>
                            </div>
                        </div> <%--end secE collapsible--%>


                    </InsertItemTemplate>
                </asp:FormView>
                <%--</div>--%>
                <%--end ref sectn--%>
            </div>
            <%--end row--%>
            <%--</div>--%>
            <%--end collapse row--%>

            <%-- END Risk fv --%>
        </ContentTemplate>

    </asp:UpdatePanel>




</asp:Content>
