﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RiskAssessApp
{
    public partial class _Default : Page
    {

        // instance EF for class level use
        private RiskAssessAppDBEntities radb = new RiskAssessAppDBEntities();

        protected void Page_Load(object sender, EventArgs e)
        {
            fvRiskAssess.Enabled = true; //referral fv
            fvNewApp.Enabled = true; //new app fv

            //if fvNewApp is nested, get it by ref:
            //FormView fvNuApp = (FormView)fvRiskAssess.FindControl("fvNewApp");


            // fvNuApp.Enabled = true;
            //For first load only, display fv in insert mode but disabled (or it'll fire after every update panel postback and stop the fv from paging properly
            // Once a px is selected in GV, put the fv in edit or insert (if no records) in the gv's selected index changed event
            if (!Page.IsPostBack)
            {
                fvRiskAssess.ChangeMode(FormViewMode.Insert);  //default mode is 'edit' which displays nothing so for first load, switch to 'insert'
                fvNewApp.ChangeMode(FormViewMode.Insert);
                //fvRVU.Enabled = false;

            }


        }


        //---------------------Risk Assess FV-----------------------------

        protected void fvRiskAssess_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            //the app save button triggers this
        }

        protected void fvRiskAssess_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {

            //grab the currently selected app id from the dd. on fv insert template (after switching to edit mode, set the edit template's dd sel value to this
            string curAppID = ((DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt")).SelectedValue;

            //check there's an app selected - if not, warn user and jump out
            if (string.IsNullOrEmpty(curAppID))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "function", "showDlg('please select an application before saving.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Please select an application before saving');", true);
                return;

            }
            

            // get fv values
            
            string radat = ((TextBox)fvRiskAssess.FindControl("txtDatRiskAss")).Text;
            string nam = ((TextBox)fvRiskAssess.FindControl("txtName")).Text;
            string dept = ((DropDownList)fvRiskAssess.FindControl("ddDept")).SelectedValue; //handle 'please select'?
            string hwage = ((TextBox)fvRiskAssess.FindControl("txtHWAge")).Text;
            string hwcond = ((DropDownList)fvRiskAssess.FindControl("ddHWCond")).SelectedValue; //handle 'please select'?
            string hwenv = ((DropDownList)fvRiskAssess.FindControl("ddHWEnviron")).SelectedValue; //handle 'please select'?
            string hwresredun = ((DropDownList)fvRiskAssess.FindControl("ddHWResilRedun")).SelectedValue; //handle 'please select'?
            string hwupd = ((DropDownList)fvRiskAssess.FindControl("ddHWUpdated")).SelectedValue; //handle 'please select'?
            string hwav = ((DropDownList)fvRiskAssess.FindControl("ddHWAV")).SelectedValue; //handle 'please select'?
            string notes = ((TextBox)fvRiskAssess.FindControl("txtNotes")).Text;
            string bms = ((DropDownList)fvRiskAssess.FindControl("ddBMS")).SelectedValue;
            string bmscmt = ((TextBox)fvRiskAssess.FindControl("txtBMSCmt")).Text;
            string bakloc = ((DropDownList)fvRiskAssess.FindControl("ddBackLoc")).SelectedValue;
            string backloccmt = ((TextBox)fvRiskAssess.FindControl("txtBackLocCmnt")).Text;
           // string rtoorpo = ((DropDownList)fvRiskAssess.FindControl("ddBackRTORPO")).SelectedValue;
           // string rtoorpocmt = ((TextBox)fvRiskAssess.FindControl("txtBackRTORPOCmnt")).Text;
            string rtotim = ((DropDownList)fvRiskAssess.FindControl("ddBackRTOTime")).SelectedValue; 
            string rpotim = ((DropDownList)fvRiskAssess.FindControl("ddBackRPOTime")).SelectedValue;
            string backarcnotmerg = ((DropDownList)fvRiskAssess.FindControl("ddBackArcNotMerged")).SelectedValue;
            string backarcnotmergcmt = ((TextBox)fvRiskAssess.FindControl("txtBackArcNotMergedCmnt")).Text;
            string rectestcomp = ((DropDownList)fvRiskAssess.FindControl("ddRecTestComplete")).SelectedValue;
            string rectestcompcmt = ((TextBox)fvRiskAssess.FindControl("txtRecTestCompleteCmnt")).Text;
            string slasupt = ((DropDownList)fvRiskAssess.FindControl("ddSLASupt")).SelectedValue;
            string slasuptcmt = ((TextBox)fvRiskAssess.FindControl("txtSLASuptCmnt")).Text;
            string kpirevd = ((DropDownList)fvRiskAssess.FindControl("ddKPIRevd")).SelectedValue;
            string kpirevdcmt = ((TextBox)fvRiskAssess.FindControl("txtKPIRevdCmnt")).Text;
            string coreappaccpol = ((DropDownList)fvRiskAssess.FindControl("ddCoreAppAccPol")).SelectedValue;
            string coreappchgmgt = ((DropDownList)fvRiskAssess.FindControl("ddCoreAppChngeMgt")).SelectedValue;
            
            //string confsupsgnd = ((DropDownList)fvRiskAssess.FindControl("ddConfSuppSgnd")).SelectedValue;
            string thrdpartyremacsgnd = ((DropDownList)fvRiskAssess.FindControl("ddThrdPartyRemAccSgnd")).SelectedValue;
        //    string thrdpartyremacsgndna = ((DropDownList)fvRiskAssess.FindControl("ddThrdPartyRemAccSgndNA")).SelectedValue;
            string appownrnam = ((TextBox)fvRiskAssess.FindControl("txtAppOwnerName")).Text; 
          //  string supcontdeets = ((DropDownList)fvRiskAssess.FindControl("ddSupportContDeets")).SelectedValue;
          //  string interdepapps = ((DropDownList)fvRiskAssess.FindControl("ddInterDepApps")).SelectedValue;
          //  string rto = ((DropDownList)fvRiskAssess.FindControl("ddRTO")).SelectedValue;
          //  string rpo = ((DropDownList)fvRiskAssess.FindControl("ddRPO")).SelectedValue;
         //   string riskratval = ((DropDownList)fvRiskAssess.FindControl("ddRiskRatVal")).SelectedValue;

         //   string datictinf = ((TextBox)fvRiskAssess.FindControl("txtDatICTInfrmd")).Text;
            string riskrat1 = ((TextBox)fvRiskAssess.FindControl("txtPt1RiskRat")).Text;
            string riskrat2 = ((TextBox)fvRiskAssess.FindControl("txtPt2RiskRat")).Text;
            string riskratoverll = ((TextBox)fvRiskAssess.FindControl("txtOverallRiskRat")).Text;
            string procpurp = ((TextBox)fvRiskAssess.FindControl("txtProcPurp")).Text;
            string procxbordtrans = ((DropDownList)fvRiskAssess.FindControl("ddProcXBordTrans")).SelectedValue;
            string procxbordtranscmt = ((TextBox)fvRiskAssess.FindControl("txtProcXBordTransHow")).Text;
            string xbordtransdat = ((TextBox)fvRiskAssess.FindControl("txtXBordTransData")).Text;
            string xbordretlen = ((TextBox)fvRiskAssess.FindControl("txtXBordRetLen")).Text;
            string xborddatstor = ((TextBox)fvRiskAssess.FindControl("txtXBordDataStore")).Text;
            string xborddatstorby = ((TextBox)fvRiskAssess.FindControl("txtXBordDataStoreBy")).Text;
            string xborddattyp = ((TextBox)fvRiskAssess.FindControl("txtXBordDataType")).Text;

            string xborddatoutsrc = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataOutsrcd")).SelectedValue;
            string xbordborddatproclst = ((TextBox)fvRiskAssess.FindControl("txtXBordDataProcList")).Text;

            string xborddatexmptsar = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataExemptSAR")).SelectedValue;
            string xborddatexmptsarrson = ((TextBox)fvRiskAssess.FindControl("txtXBordDataExemptSARRson")).Text;

            string xborddatsafe = ((TextBox)fvRiskAssess.FindControl("txtXBordDataSafe")).Text;

            string xborddatrightforget = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataRightForget")).SelectedValue;
            string xborddatprevproc = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataRightPreventProc")).SelectedValue;
            string xborddataudlogpxdat = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataAuditLogPxData")).SelectedValue;
            string xbordcomplaw = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyLaw")).SelectedValue;
            string xbordcomppurplmt = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyPurpLmt")).SelectedValue;
            string xbordcompdatmin = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyDataMin")).SelectedValue;
            string xbordcompaccur = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyAccur")).SelectedValue;
            string xbordcompstrglim = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyStrgLim")).SelectedValue;
            string xbordcompintconf = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyIntConf")).SelectedValue;
            string xbordclasssens = ((DropDownList)fvRiskAssess.FindControl("ddXBordClassSens")).SelectedValue;
            string xbordprocaddend = ((DropDownList)fvRiskAssess.FindControl("ddXBordProcAddend")).SelectedValue;


            // update existing record
            //nb: if this update is coming via the dept filter dd then fv's 'selectedValue' isn't available and must use the session to get it.

            //try getting current fv record id via selval prop:
            int curID;

            if (fvRiskAssess.SelectedValue != null)
            {
               curID = (int)fvRiskAssess.SelectedValue;
            }
            else
            {
                curID = (int)Session["curRAID"] ;
            }
            

            tblRiskAssess raUpd = radb.tblRiskAssesses.Where(ra => ra.ID == curID).First();  //get the existing record into an object
            
            //populate the raUpd object and save...

            raUpd.DateRiskAssess = string.IsNullOrEmpty(radat) ? (DateTime?)null : DateTime.Parse(radat);  //datetime with terniary null date handler
            raUpd.Username = nam;
           // raUpd.Dept = string.IsNullOrEmpty(dept) ? (int?)null : int.Parse(dept);  //handle if null - nb: ensure a dept is selected
            raUpd.AgeHardware = hwage;
            raUpd.HardwareCondition = hwcond;
            raUpd.HardwareEnviron = hwenv;
            raUpd.HardwareResilRedun = hwresredun;
            raUpd.HardwareUpdated = hwupd;
            raUpd.HardwareAV = hwav;
            raUpd.Notes = notes;
            raUpd.BackupMediaSecure = bms;
            raUpd.BackupMediaSecureComment = bmscmt;
            raUpd.BackupReqLocation = bakloc;
            raUpd.BackupReqLocationComment = backloccmt;
            //raUpd.BackupRTORPO = rtoorpo;
            //raUpd.BackupRTORPOComment = rtoorpocmt;
            raUpd.BackupRPODefined = rpotim;
            raUpd.BackupRTODefined = rtotim;
            raUpd.BackupsArchiveNotMerged = backarcnotmerg;
            raUpd.BackupsArchiveNotMergedComment = backarcnotmergcmt;
            raUpd.RecoverabilityTestCompleted = rectestcomp;
            raUpd.RecoverabilityTestCompletedComment = rectestcompcmt;
            raUpd.SLASupport = slasupt;
            raUpd.SLASupportComment = slasuptcmt;
            raUpd.SLAKPIReviewed = kpirevd;
            raUpd.SLAKPIReviewedComment = kpirevdcmt;
            raUpd.CoreAppAccessPolicy = coreappaccpol;
            raUpd.CoreAppChangeMgt = coreappchgmgt;
          //  raUpd.ConfidentSupplierSigned = confsupsgnd;
            raUpd.ThirdPartyRemAccSigned = thrdpartyremacsgnd;
          //  raUpd.ThirdPartyRemAccSignedNA = thrdpartyremacsgndna;
            raUpd.AppOwnerName = appownrnam;
         //   raUpd.SupportContactDetails = supcontdeets;
         //   raUpd.InterdependentApps = interdepapps;
        //    raUpd.RTO = rto;
        //    raUpd.RPO = rpo;
        //    raUpd.RiskRatingValue = riskratval;
        //    raUpd.DateICTInformed = string.IsNullOrEmpty(datictinf) ? (DateTime?)null : DateTime.Parse(datictinf);
            raUpd.Part1RiskRating = string.IsNullOrEmpty(riskrat1) ? (int?)null : int.Parse(riskrat1);
            raUpd.Part2RiskRating = string.IsNullOrEmpty(riskrat2) ? (int?)null : int.Parse(riskrat2);
            raUpd.OverallRiskRating = string.IsNullOrEmpty(riskratoverll) ? (int?)null : int.Parse(riskratoverll);
            raUpd.ProcessPurpose = procpurp;
            raUpd.ProcessCrossBorderTrans = procxbordtrans;
            raUpd.CrossBorderHowDone = procxbordtranscmt;
            raUpd.CrossBorderTransData = xbordtransdat;
            raUpd.CrossBorderTransDataRetainLength = xbordretlen;
            raUpd.CrossBorderTransDataStorage = xborddatstor;
            raUpd.CrossBorderTransDataStorageBy = xborddatstorby;
            raUpd.CrossBorderTransDataType = xborddattyp;
            raUpd.CrossBorderTransDataOutsourced = xborddatoutsrc;
            raUpd.CrossBorderTransDataProcessorList = xbordborddatproclst;
            raUpd.CrossBorderTransDataExemptSAR = xborddatexmptsar;
            raUpd.CrossBorderTransDataExemptSARReason = xborddatexmptsarrson;
            raUpd.CrossBorderTransDataSafeguards = xborddatsafe;
            raUpd.CrossBorderTransDataRightForgotten = xborddatrightforget;
            raUpd.CrossBorderTransDataRightPreventProcess = xborddatprevproc;
            raUpd.CrossBorderTransAuditLogPxDataAccess = xborddataudlogpxdat;
            raUpd.CrossBorderTransDataComplyLawfulness = xbordcomplaw;
            raUpd.CrossBorderTransDataComplyPurposeLimit = xbordcomppurplmt;
            raUpd.CrossBorderTransDataComplyDataMin = xbordcompdatmin;
            raUpd.CrossBorderTransDataComplyIntegAccuracy = xbordcompaccur;
            raUpd.CrossBorderTransDataComplyStorageLimit = xbordcompstrglim;
            raUpd.CrossBorderTransDataComplyIntegConfident = xbordcompintconf;
            raUpd.CrossBorderTransDataClassifiedSensitive = xbordclasssens;
            raUpd.CrossBorderTransDataProcessorAddendum = xbordprocaddend;


            //dayUpd.PainAtRest = string.IsNullOrEmpty(PAR) ? (int?)null : int.Parse(PAR); //string wth terniary null value handler
            //bool open = ((CheckBox)fvSummary.FindControl("SurgTypeOpenCheckBox")).Checked;  //checkbox

            radb.SaveChanges();  //save changes

            //save to RA history table (always inserts - no updates)
            tblRiskAssessHistory raHx = new tblRiskAssessHistory();
            raHx.RAID = curID;
            raHx.DateRiskAssess = string.IsNullOrEmpty(radat) ? (DateTime?)null : DateTime.Parse(radat);  //datetime with terniary null date handler
            raHx.AppName = int.Parse(curAppID);
            raHx.Username = nam;
            raHx.Dept = string.IsNullOrEmpty(dept) ? (int?)null : int.Parse(dept);  //handle if null - nb: ensure a dept is selected
            raHx.AgeHardware = hwage;
            raHx.HardwareCondition = hwcond;
            raHx.HardwareEnviron = hwenv;
            raHx.HardwareResilRedun = hwresredun;
            raHx.HardwareUpdated = hwupd;
            raHx.HardwareAV = hwav;
            raHx.Notes = notes;
            raHx.BackupMediaSecure = bms;
            raHx.BackupMediaSecureComment = bmscmt;
            raHx.BackupReqLocation = bakloc;
            raHx.BackupReqLocationComment = backloccmt;
            //raHx.BackupRTORPO = rtoorpo;
            //raHx.BackupRTORPOComment = rtoorpocmt;
            raHx.BackupRPODefined = rpotim;
            raHx.BackupRTODefined = rtotim;
            raHx.BackupsArchiveNotMerged = backarcnotmerg;
            raHx.BackupsArchiveNotMergedComment = backarcnotmergcmt;
            raHx.RecoverabilityTestCompleted = rectestcomp;
            raHx.RecoverabilityTestCompletedComment = rectestcompcmt;
            raHx.SLASupport = slasupt;
            raHx.SLASupportComment = slasuptcmt;
            raHx.SLAKPIReviewed = kpirevd;
            raHx.SLAKPIReviewedComment = kpirevdcmt;
            raHx.CoreAppAccessPolicy = coreappaccpol;
            raHx.CoreAppChangeMgt = coreappchgmgt;
          //  raHx.ConfidentSupplierSigned = confsupsgnd;
            raHx.ThirdPartyRemAccSigned = thrdpartyremacsgnd;
        //    raHx.ThirdPartyRemAccSignedNA = thrdpartyremacsgndna;
            raHx.AppOwnerName = appownrnam;
       //     raHx.SupportContactDetails = supcontdeets;
       //     raHx.InterdependentApps = interdepapps;
       //     raHx.RTO = rto;
       //     raHx.RPO = rpo;
       //     raHx.RiskRatingValue = riskratval;
      //      raHx.DateICTInformed = string.IsNullOrEmpty(datictinf) ? (DateTime?)null : DateTime.Parse(datictinf);
            raHx.Part1RiskRating = string.IsNullOrEmpty(riskrat1) ? (int?)null : int.Parse(riskrat1);
            raHx.Part2RiskRating = string.IsNullOrEmpty(riskrat2) ? (int?)null : int.Parse(riskrat2);
            raHx.OverallRiskRating = string.IsNullOrEmpty(riskratoverll) ? (int?)null : int.Parse(riskratoverll);
            raHx.ProcessPurpose = procpurp;
            raHx.ProcessCrossBorderTrans = procxbordtrans;
            raHx.CrossBorderHowDone = procxbordtranscmt;
            raHx.CrossBorderTransData = xbordtransdat;
            raHx.CrossBorderTransDataRetainLength = xbordretlen;
            raHx.CrossBorderTransDataStorage = xborddatstor;
            raHx.CrossBorderTransDataStorageBy = xborddatstorby;
            raHx.CrossBorderTransDataType = xborddattyp;
            raHx.CrossBorderTransDataOutsourced = xborddatoutsrc;
            raHx.CrossBorderTransDataProcessorList = xbordborddatproclst;
            raHx.CrossBorderTransDataExemptSAR = xborddatexmptsar;
            raHx.CrossBorderTransDataExemptSARReason = xborddatexmptsarrson;
            raHx.CrossBorderTransDataSafeguards = xborddatsafe;
            raHx.CrossBorderTransDataRightForgotten = xborddatrightforget;
            raHx.CrossBorderTransDataRightPreventProcess = xborddatprevproc;
            raHx.CrossBorderTransAuditLogPxDataAccess = xborddataudlogpxdat;
            raHx.CrossBorderTransDataComplyLawfulness = xbordcomplaw;
            raHx.CrossBorderTransDataComplyPurposeLimit = xbordcomppurplmt;
            raHx.CrossBorderTransDataComplyDataMin = xbordcompdatmin;
            raHx.CrossBorderTransDataComplyIntegAccuracy = xbordcompaccur;
            raHx.CrossBorderTransDataComplyStorageLimit = xbordcompstrglim;
            raHx.CrossBorderTransDataComplyIntegConfident = xbordcompintconf;
            raHx.CrossBorderTransDataClassifiedSensitive = xbordclasssens;
            raHx.CrossBorderTransDataProcessorAddendum = xbordprocaddend;
            raHx.DateUpdated = DateTime.Now;
            raHx.LoggedInUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            raHx.PCHost = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).HostName;

            radb.tblRiskAssessHistories.Add(raHx);
            radb.SaveChanges();  //save changes

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('Item Updated');", true);
        }

        protected void fvRiskAssess_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
        {

        }

        protected void fvRiskAssess_ItemInserting(object sender, FormViewInsertEventArgs e)
        {

            //grab the currently selected app id from the dd. on fv insert template (after switching to edit mode, set the edit template's dd sel value to this
            string curAppID = ((DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt")).SelectedValue;
            string deptID = ((DropDownList)fvRiskAssess.FindControl("ddDept")).SelectedValue;

            if (string.IsNullOrEmpty(curAppID) || string.IsNullOrEmpty(deptID))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "function", "showDlg('please select an Application and Department before saving.');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Please select an application before saving');", true);
                return;

            }

            // get fv values
            
            //orig:

            //string radat = ((TextBox)fvRiskAssess.FindControl("txtDatRiskAss")).Text;
            //string nam = ((TextBox)fvRiskAssess.FindControl("txtName")).Text;
            //string dept = ((DropDownList)fvRiskAssess.FindControl("ddDept")).SelectedValue; //handle 'please select'?
            //string hwage = ((TextBox)fvRiskAssess.FindControl("txtHWAge")).Text;
            //string hwcond = ((DropDownList)fvRiskAssess.FindControl("ddHWCond")).SelectedValue; //handle 'please select'?
            //string hwenv = ((DropDownList)fvRiskAssess.FindControl("ddHWEnviron")).SelectedValue; //handle 'please select'?
            //string hwresredun = ((DropDownList)fvRiskAssess.FindControl("ddHWResilRedun")).SelectedValue; //handle 'please select'?
            //string hwupd = ((DropDownList)fvRiskAssess.FindControl("ddHWUpdated")).SelectedValue; //handle 'please select'?
            //string hwav = ((DropDownList)fvRiskAssess.FindControl("ddHWAV")).SelectedValue; //handle 'please select'?
            //bool bms = ((CheckBox)fvRiskAssess.FindControl("chkBMS")).Checked;
            //string bmscmt = ((TextBox)fvRiskAssess.FindControl("txtBMSCmt")).Text;
            //bool bakloc = ((CheckBox)fvRiskAssess.FindControl("chkBackLoc")).Checked;
            //string backloccmt = ((TextBox)fvRiskAssess.FindControl("txtBackLocCmnt")).Text;
            //bool rtoorpo = ((CheckBox)fvRiskAssess.FindControl("chkBackRTORPO")).Checked;
            //string rtoorpocmt = ((TextBox)fvRiskAssess.FindControl("txtBackRTORPOCmnt")).Text;
            //string rtotim = ((TextBox)fvRiskAssess.FindControl("txtBackRTOTime")).Text;
            //string rpotim = ((TextBox)fvRiskAssess.FindControl("txtBackRPOTime")).Text;
            //bool backarcnotmerg = ((CheckBox)fvRiskAssess.FindControl("chkBackArcNotMerged")).Checked;
            //string backarcnotmergcmt = ((TextBox)fvRiskAssess.FindControl("txtBackArcNotMergedCmnt")).Text;
            //bool rectestcomp = ((CheckBox)fvRiskAssess.FindControl("chkRecTestComplete")).Checked;
            //string rectestcompcmt = ((TextBox)fvRiskAssess.FindControl("txtRecTestCompleteCmnt")).Text;
            //bool slasupt = ((CheckBox)fvRiskAssess.FindControl("chkSLASupt")).Checked;
            //string slasuptcmt = ((TextBox)fvRiskAssess.FindControl("txtSLASuptCmnt")).Text;
            //bool kpirevd = ((CheckBox)fvRiskAssess.FindControl("chkKPIRevd")).Checked;
            //string kpirevdcmt = ((TextBox)fvRiskAssess.FindControl("txtKPIRevdCmnt")).Text;
            //bool coreappaccpol = ((CheckBox)fvRiskAssess.FindControl("chkCoreAppAccPol")).Checked;
            //bool coreappchgmgt = ((CheckBox)fvRiskAssess.FindControl("chkCoreAppChngeMgt")).Checked;

            //bool confsupsgnd = ((CheckBox)fvRiskAssess.FindControl("chkConfSuppSgnd")).Checked;
            //bool thrdpartyremacsgnd = ((CheckBox)fvRiskAssess.FindControl("chkThrdPartyRemAccSgnd")).Checked;
            //bool thrdpartyremacsgndna = ((CheckBox)fvRiskAssess.FindControl("chkThrdPartyRemAccSgndNA")).Checked;
            //bool appownrnam = ((CheckBox)fvRiskAssess.FindControl("chkAppOwnrNam")).Checked;
            //bool supcontdeets = ((CheckBox)fvRiskAssess.FindControl("chkSupportContDeets")).Checked;
            //bool interdepapps = ((CheckBox)fvRiskAssess.FindControl("chkInterDepApps")).Checked;
            //bool rto = ((CheckBox)fvRiskAssess.FindControl("chkRTO")).Checked;
            //bool rpo = ((CheckBox)fvRiskAssess.FindControl("chkRPO")).Checked;
            //bool riskratval = ((CheckBox)fvRiskAssess.FindControl("chkRiskRatVal")).Checked;

            //string datictinf = ((TextBox)fvRiskAssess.FindControl("txtDatICTInfrmd")).Text;
            //string riskrat1 = ((TextBox)fvRiskAssess.FindControl("txtPt1RiskRat")).Text;
            //string riskrat2 = ((TextBox)fvRiskAssess.FindControl("txtPt2RiskRat")).Text;
            //string riskratoverll = ((TextBox)fvRiskAssess.FindControl("txtOverallRiskRat")).Text;
            //string procpurp = ((TextBox)fvRiskAssess.FindControl("txtProcPurp")).Text;
            //bool procxbordtrans = ((CheckBox)fvRiskAssess.FindControl("chkProcXBordTrans")).Checked;
            //string procxbordtranscmt = ((TextBox)fvRiskAssess.FindControl("txtProcXBordTransHow")).Text;
            //string xbordtransdat = ((TextBox)fvRiskAssess.FindControl("txtXBordTransData")).Text;
            //string xbordretlen = ((TextBox)fvRiskAssess.FindControl("txtXBordRetLen")).Text;
            //string xborddatstor = ((TextBox)fvRiskAssess.FindControl("txtXBordDataStore")).Text;
            //string xborddatstorby = ((TextBox)fvRiskAssess.FindControl("txtXBordDataStoreBy")).Text;
            //string xborddattyp = ((TextBox)fvRiskAssess.FindControl("txtXBordDataType")).Text;

            //bool xborddatoutsrc = ((CheckBox)fvRiskAssess.FindControl("chkXBordDataOutsrcd")).Checked;
            //string xbordborddatproclst = ((TextBox)fvRiskAssess.FindControl("txtXBordDataProcList")).Text;

            //bool xborddatexmptsar = ((CheckBox)fvRiskAssess.FindControl("chkXBordDataExemptSAR")).Checked;
            //string xborddatexmptsarrson = ((TextBox)fvRiskAssess.FindControl("txtXBordDataExemptSARRson")).Text;

            //string xborddatsafe = ((TextBox)fvRiskAssess.FindControl("txtXBordDataSafe")).Text;

            //bool xborddatrightforget = ((CheckBox)fvRiskAssess.FindControl("chkXBordDataRightForget")).Checked;
            //bool xborddatprevproc = ((CheckBox)fvRiskAssess.FindControl("chkXBordDataRightPreventProc")).Checked;
            //bool xborddataudlogpxdat = ((CheckBox)fvRiskAssess.FindControl("chkXBordDataAuditLogPxData")).Checked;
            //bool xbordcomplaw = ((CheckBox)fvRiskAssess.FindControl("chkXBordComplyLaw")).Checked;
            //bool xbordcomppurplmt = ((CheckBox)fvRiskAssess.FindControl("chkXBordComplyPurpLmt")).Checked;
            //bool xbordcompdatmin = ((CheckBox)fvRiskAssess.FindControl("chkXBordComplyDataMin")).Checked;
            //bool xbordcompaccur = ((CheckBox)fvRiskAssess.FindControl("chkXBordComplyAccur")).Checked;
            //bool xbordcompstrglim = ((CheckBox)fvRiskAssess.FindControl("chkXBordComplyStrgLim")).Checked;
            //bool xbordcompintconf = ((CheckBox)fvRiskAssess.FindControl("chkXBordComplyIntConf")).Checked;
            //bool xbordclasssens = ((CheckBox)fvRiskAssess.FindControl("chkXBordClassSens")).Checked;
            //bool xbordprocaddend = ((CheckBox)fvRiskAssess.FindControl("chkXBordProcAddend")).Checked;

            string radat = ((TextBox)fvRiskAssess.FindControl("txtDatRiskAss")).Text;
            string nam = ((TextBox)fvRiskAssess.FindControl("txtName")).Text;
            string dept = ((DropDownList)fvRiskAssess.FindControl("ddDept")).SelectedValue; //handle 'please select'?
            string hwage = ((TextBox)fvRiskAssess.FindControl("txtHWAge")).Text;
            string hwcond = ((DropDownList)fvRiskAssess.FindControl("ddHWCond")).SelectedValue; //handle 'please select'?
            string hwenv = ((DropDownList)fvRiskAssess.FindControl("ddHWEnviron")).SelectedValue; //handle 'please select'?
            string hwresredun = ((DropDownList)fvRiskAssess.FindControl("ddHWResilRedun")).SelectedValue; //handle 'please select'?
            string hwupd = ((DropDownList)fvRiskAssess.FindControl("ddHWUpdated")).SelectedValue; //handle 'please select'?
            string hwav = ((DropDownList)fvRiskAssess.FindControl("ddHWAV")).SelectedValue; //handle 'please select'?
            string bms = ((DropDownList)fvRiskAssess.FindControl("ddBMS")).SelectedValue;
            string bmscmt = ((TextBox)fvRiskAssess.FindControl("txtBMSCmt")).Text;
            string notes = ((TextBox)fvRiskAssess.FindControl("txtNotes")).Text;
            string bakloc = ((DropDownList)fvRiskAssess.FindControl("ddBackLoc")).SelectedValue;
            string backloccmt = ((TextBox)fvRiskAssess.FindControl("txtBackLocCmnt")).Text;
           // string rtoorpo = ((DropDownList)fvRiskAssess.FindControl("ddBackRTORPO")).SelectedValue;
           // string rtoorpocmt = ((TextBox)fvRiskAssess.FindControl("txtBackRTORPOCmnt")).Text;
            string rtotim = ((DropDownList)fvRiskAssess.FindControl("ddBackRTOTime")).SelectedValue;
            string rpotim = ((DropDownList)fvRiskAssess.FindControl("ddBackRPOTime")).SelectedValue;
            string backarcnotmerg = ((DropDownList)fvRiskAssess.FindControl("ddBackArcNotMerged")).SelectedValue;
            string backarcnotmergcmt = ((TextBox)fvRiskAssess.FindControl("txtBackArcNotMergedCmnt")).Text;
            string rectestcomp = ((DropDownList)fvRiskAssess.FindControl("ddRecTestComplete")).SelectedValue;
            string rectestcompcmt = ((TextBox)fvRiskAssess.FindControl("txtRecTestCompleteCmnt")).Text;
            string slasupt = ((DropDownList)fvRiskAssess.FindControl("ddSLASupt")).SelectedValue;
            string slasuptcmt = ((TextBox)fvRiskAssess.FindControl("txtSLASuptCmnt")).Text;
            string kpirevd = ((DropDownList)fvRiskAssess.FindControl("ddKPIRevd")).SelectedValue;
            string kpirevdcmt = ((TextBox)fvRiskAssess.FindControl("txtKPIRevdCmnt")).Text;
            string coreappaccpol = ((DropDownList)fvRiskAssess.FindControl("ddCoreAppAccPol")).SelectedValue;
            string coreappchgmgt = ((DropDownList)fvRiskAssess.FindControl("ddCoreAppChngeMgt")).SelectedValue;

           // string confsupsgnd = ((DropDownList)fvRiskAssess.FindControl("ddConfSuppSgnd")).SelectedValue;
            string thrdpartyremacsgnd = ((DropDownList)fvRiskAssess.FindControl("ddThrdPartyRemAccSgnd")).SelectedValue;
          //  string thrdpartyremacsgndna = ((DropDownList)fvRiskAssess.FindControl("ddThrdPartyRemAccSgndNA")).SelectedValue;
            string appownrnam = ((TextBox)fvRiskAssess.FindControl("txtAppOwnerName")).Text;
          //  string supcontdeets = ((DropDownList)fvRiskAssess.FindControl("ddSupportContDeets")).SelectedValue;
          //  string interdepapps = ((DropDownList)fvRiskAssess.FindControl("ddInterDepApps")).SelectedValue;
          //  string rto = ((DropDownList)fvRiskAssess.FindControl("ddRTO")).SelectedValue;
          //  string rpo = ((DropDownList)fvRiskAssess.FindControl("ddRPO")).SelectedValue;
          //  string riskratval = ((DropDownList)fvRiskAssess.FindControl("ddRiskRatVal")).SelectedValue;

          //  string datictinf = ((TextBox)fvRiskAssess.FindControl("txtDatICTInfrmd")).Text;
            string riskrat1 = ((TextBox)fvRiskAssess.FindControl("txtPt1RiskRat")).Text;
            string riskrat2 = ((TextBox)fvRiskAssess.FindControl("txtPt2RiskRat")).Text;
            string riskratoverll = ((TextBox)fvRiskAssess.FindControl("txtOverallRiskRat")).Text;
            string procpurp = ((TextBox)fvRiskAssess.FindControl("txtProcPurp")).Text;
            string procxbordtrans = ((DropDownList)fvRiskAssess.FindControl("ddProcXBordTrans")).SelectedValue;
            string procxbordtranscmt = ((TextBox)fvRiskAssess.FindControl("txtProcXBordTransHow")).Text;
            string xbordtransdat = ((TextBox)fvRiskAssess.FindControl("txtXBordTransData")).Text;
            string xbordretlen = ((TextBox)fvRiskAssess.FindControl("txtXBordRetLen")).Text;
            string xborddatstor = ((TextBox)fvRiskAssess.FindControl("txtXBordDataStore")).Text;
            string xborddatstorby = ((TextBox)fvRiskAssess.FindControl("txtXBordDataStoreBy")).Text;
            string xborddattyp = ((TextBox)fvRiskAssess.FindControl("txtXBordDataType")).Text;

            string xborddatoutsrc = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataOutsrcd")).SelectedValue;
            string xbordborddatproclst = ((TextBox)fvRiskAssess.FindControl("txtXBordDataProcList")).Text;

            string xborddatexmptsar = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataExemptSAR")).SelectedValue;
            string xborddatexmptsarrson = ((TextBox)fvRiskAssess.FindControl("txtXBordDataExemptSARRson")).Text;

            string xborddatsafe = ((TextBox)fvRiskAssess.FindControl("txtXBordDataSafe")).Text;

            string xborddatrightforget = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataRightForget")).SelectedValue;
            string xborddatprevproc = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataRightPreventProc")).SelectedValue;
            string xborddataudlogpxdat = ((DropDownList)fvRiskAssess.FindControl("ddXBordDataAuditLogPxData")).SelectedValue;
            string xbordcomplaw = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyLaw")).SelectedValue;
            string xbordcomppurplmt = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyPurpLmt")).SelectedValue;
            string xbordcompdatmin = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyDataMin")).SelectedValue;
            string xbordcompaccur = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyAccur")).SelectedValue;
            string xbordcompstrglim = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyStrgLim")).SelectedValue;
            string xbordcompintconf = ((DropDownList)fvRiskAssess.FindControl("ddXBordComplyIntConf")).SelectedValue;
            string xbordclasssens = ((DropDownList)fvRiskAssess.FindControl("ddXBordClassSens")).SelectedValue;
            string xbordprocaddend = ((DropDownList)fvRiskAssess.FindControl("ddXBordProcAddend")).SelectedValue;


            //create new ra object and populate it

            tblRiskAssess raNu = new tblRiskAssess();

            raNu.AppName = int.Parse(curAppID);
            raNu.DateRiskAssess = string.IsNullOrEmpty(radat) ? (DateTime?)null : DateTime.Parse(radat);  //datetime with terniary null date handler
            raNu.Username = nam;
            raNu.Dept = string.IsNullOrEmpty(dept) ? (int?)null : int.Parse(dept);  //handle if null - nb: ensure a dept is selected
            raNu.AgeHardware = hwage;
            raNu.HardwareCondition = hwcond;
            raNu.HardwareEnviron = hwenv;
            raNu.HardwareResilRedun = hwresredun;
            raNu.HardwareUpdated = hwupd;
            raNu.HardwareAV = hwav;
            raNu.Notes = notes;
            raNu.BackupMediaSecure = bms;
            raNu.BackupMediaSecureComment = bmscmt;
            raNu.BackupReqLocation = bakloc;
            raNu.BackupReqLocationComment = backloccmt;
         //   raNu.BackupRTORPO = rtoorpo;
         //   raNu.BackupRTORPOComment = rtoorpocmt;
            raNu.BackupRPODefined = rpotim;
            raNu.BackupRTODefined = rtotim;
            raNu.BackupsArchiveNotMerged = backarcnotmerg;
            raNu.BackupsArchiveNotMergedComment = backarcnotmergcmt;
            raNu.RecoverabilityTestCompleted = rectestcomp;
            raNu.RecoverabilityTestCompletedComment = rectestcompcmt;
            raNu.SLASupport = slasupt;
            raNu.SLASupportComment = slasuptcmt;
            raNu.SLAKPIReviewed = kpirevd;
            raNu.SLAKPIReviewedComment = kpirevdcmt;
            raNu.CoreAppAccessPolicy = coreappaccpol;
            raNu.CoreAppChangeMgt = coreappchgmgt;
       //     raNu.ConfidentSupplierSigned = confsupsgnd;
            raNu.ThirdPartyRemAccSigned = thrdpartyremacsgnd;
       //     raNu.ThirdPartyRemAccSignedNA = thrdpartyremacsgndna;
            raNu.AppOwnerName = appownrnam;
       //     raNu.SupportContactDetails = supcontdeets;
      //      raNu.InterdependentApps = interdepapps;
      //      raNu.RTO = rto;
     //       raNu.RPO = rpo;
     //       raNu.RiskRatingValue = riskratval;
     //       raNu.DateICTInformed = string.IsNullOrEmpty(datictinf) ? (DateTime?)null : DateTime.Parse(datictinf);
            raNu.Part1RiskRating = string.IsNullOrEmpty(riskrat1) ? (int?)null : int.Parse(riskrat1);
            raNu.Part2RiskRating = string.IsNullOrEmpty(riskrat2) ? (int?)null : int.Parse(riskrat2);
            raNu.OverallRiskRating = string.IsNullOrEmpty(riskratoverll) ? (int?)null : int.Parse(riskratoverll);
            raNu.ProcessPurpose = procpurp;
            raNu.ProcessCrossBorderTrans = procxbordtrans;
            raNu.CrossBorderHowDone = procxbordtranscmt;
            raNu.CrossBorderTransData = xbordtransdat;
            raNu.CrossBorderTransDataRetainLength = xbordretlen;
            raNu.CrossBorderTransDataStorage = xborddatstor;
            raNu.CrossBorderTransDataStorageBy = xborddatstorby;
            raNu.CrossBorderTransDataType = xborddattyp;
            raNu.CrossBorderTransDataOutsourced = xborddatoutsrc;
            raNu.CrossBorderTransDataProcessorList = xbordborddatproclst;
            raNu.CrossBorderTransDataExemptSAR = xborddatexmptsar;
            raNu.CrossBorderTransDataExemptSARReason = xborddatexmptsarrson;
            raNu.CrossBorderTransDataSafeguards = xborddatsafe;
            raNu.CrossBorderTransDataRightForgotten = xborddatrightforget;
            raNu.CrossBorderTransDataRightPreventProcess = xborddatprevproc;
            raNu.CrossBorderTransAuditLogPxDataAccess = xborddataudlogpxdat;
            raNu.CrossBorderTransDataComplyLawfulness = xbordcomplaw;
            raNu.CrossBorderTransDataComplyPurposeLimit = xbordcomppurplmt;
            raNu.CrossBorderTransDataComplyDataMin = xbordcompdatmin;
            raNu.CrossBorderTransDataComplyIntegAccuracy = xbordcompaccur;
            raNu.CrossBorderTransDataComplyStorageLimit = xbordcompstrglim;
            raNu.CrossBorderTransDataComplyIntegConfident = xbordcompintconf;
            raNu.CrossBorderTransDataClassifiedSensitive = xbordclasssens;
            raNu.CrossBorderTransDataProcessorAddendum = xbordprocaddend;




            radb.tblRiskAssesses.Add(raNu);
            radb.SaveChanges();

            //save to RA history table (always inserts - no updates)
            tblRiskAssessHistory raHx = new tblRiskAssessHistory();
            raHx.RAID = raNu.ID; //pull id from newly inserted record above
            raHx.DateRiskAssess = string.IsNullOrEmpty(radat) ? (DateTime?)null : DateTime.Parse(radat);  //datetime with terniary null date handler
            raHx.AppName = int.Parse(curAppID);
            raHx.Username = nam;
            raHx.Dept = string.IsNullOrEmpty(dept) ? (int?)null : int.Parse(dept);  //handle if null - nb: ensure a dept is selected
            raHx.AgeHardware = hwage;
            raHx.HardwareCondition = hwcond;
            raHx.HardwareEnviron = hwenv;
            raHx.HardwareResilRedun = hwresredun;
            raHx.HardwareUpdated = hwupd;
            raHx.HardwareAV = hwav;
            raHx.Notes = notes;
            raHx.BackupMediaSecure = bms;
            raHx.BackupMediaSecureComment = bmscmt;
            raHx.BackupReqLocation = bakloc;
            raHx.BackupReqLocationComment = backloccmt;
       //     raHx.BackupRTORPO = rtoorpo;
      //      raHx.BackupRTORPOComment = rtoorpocmt;
            raHx.BackupRPODefined = rpotim;
            raHx.BackupRTODefined = rtotim;
            raHx.BackupsArchiveNotMerged = backarcnotmerg;
            raHx.BackupsArchiveNotMergedComment = backarcnotmergcmt;
            raHx.RecoverabilityTestCompleted = rectestcomp;
            raHx.RecoverabilityTestCompletedComment = rectestcompcmt;
            raHx.SLASupport = slasupt;
            raHx.SLASupportComment = slasuptcmt;
            raHx.SLAKPIReviewed = kpirevd;
            raHx.SLAKPIReviewedComment = kpirevdcmt;
            raHx.CoreAppAccessPolicy = coreappaccpol;
            raHx.CoreAppChangeMgt = coreappchgmgt;
      //      raHx.ConfidentSupplierSigned = confsupsgnd;
            raHx.ThirdPartyRemAccSigned = thrdpartyremacsgnd;
     //       raHx.ThirdPartyRemAccSignedNA = thrdpartyremacsgndna;
            raHx.AppOwnerName = appownrnam;
     //       raHx.SupportContactDetails = supcontdeets;
     //       raHx.InterdependentApps = interdepapps;
     //       raHx.RTO = rto;
     //       raHx.RPO = rpo;
     //       raHx.RiskRatingValue = riskratval;
     //       raHx.DateICTInformed = string.IsNullOrEmpty(datictinf) ? (DateTime?)null : DateTime.Parse(datictinf);
            raHx.Part1RiskRating = string.IsNullOrEmpty(riskrat1) ? (int?)null : int.Parse(riskrat1);
            raHx.Part2RiskRating = string.IsNullOrEmpty(riskrat2) ? (int?)null : int.Parse(riskrat2);
            raHx.OverallRiskRating = string.IsNullOrEmpty(riskratoverll) ? (int?)null : int.Parse(riskratoverll);
            raHx.ProcessPurpose = procpurp;
            raHx.ProcessCrossBorderTrans = procxbordtrans;
            raHx.CrossBorderHowDone = procxbordtranscmt;
            raHx.CrossBorderTransData = xbordtransdat;
            raHx.CrossBorderTransDataRetainLength = xbordretlen;
            raHx.CrossBorderTransDataStorage = xborddatstor;
            raHx.CrossBorderTransDataStorageBy = xborddatstorby;
            raHx.CrossBorderTransDataType = xborddattyp;
            raHx.CrossBorderTransDataOutsourced = xborddatoutsrc;
            raHx.CrossBorderTransDataProcessorList = xbordborddatproclst;
            raHx.CrossBorderTransDataExemptSAR = xborddatexmptsar;
            raHx.CrossBorderTransDataExemptSARReason = xborddatexmptsarrson;
            raHx.CrossBorderTransDataSafeguards = xborddatsafe;
            raHx.CrossBorderTransDataRightForgotten = xborddatrightforget;
            raHx.CrossBorderTransDataRightPreventProcess = xborddatprevproc;
            raHx.CrossBorderTransAuditLogPxDataAccess = xborddataudlogpxdat;
            raHx.CrossBorderTransDataComplyLawfulness = xbordcomplaw;
            raHx.CrossBorderTransDataComplyPurposeLimit = xbordcomppurplmt;
            raHx.CrossBorderTransDataComplyDataMin = xbordcompdatmin;
            raHx.CrossBorderTransDataComplyIntegAccuracy = xbordcompaccur;
            raHx.CrossBorderTransDataComplyStorageLimit = xbordcompstrglim;
            raHx.CrossBorderTransDataComplyIntegConfident = xbordcompintconf;
            raHx.CrossBorderTransDataClassifiedSensitive = xbordclasssens;
            raHx.CrossBorderTransDataProcessorAddendum = xbordprocaddend;
            raHx.DateUpdated = DateTime.Now;
            raHx.LoggedInUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            raHx.PCHost = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).HostName;


            //host name. - TODO: create the field and update the EF 
            //raHx.userpc = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).HostName;

            radb.tblRiskAssessHistories.Add(raHx);
            radb.SaveChanges();

            RebindRiskAssessFvForSelApp(int.Parse(curAppID));

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('Item Saved');", true);
            //....then display latest record in edit mode:
           // var selSummaryCmd = radb.tblRiskAssesses.Where(ra => ra.ID == raNu.ID).ToList();
          //  fvRiskAssess.DataSource = selSummaryCmd;

            ////fvRVU.PageIndex = selSummaryCmd.Count();   //to display latest record if fv is multirecord

        }

        protected void fvRiskAssess_ItemInserted(object sender, FormViewInsertedEventArgs e)
        {

            //keep in edit mode...
            //e.KeepInInsertMode = true;
            //fvRiskAssess.ChangeMode(FormViewMode.Edit);

            //DropDownList ddApps = (DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt");
            //ddApps.DataBind();

        }

        protected void fvRiskAssess_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            //handle the mode changing programmatically (automatic when using sqlds)
            fvRiskAssess.ChangeMode(e.NewMode);
        }

        protected void fvRiskAssess_PageIndexChanging(object sender, FormViewPageEventArgs e)
        {
            int curDeptID = int.Parse(ddDeptFilter.SelectedValue);

            // working var selcmd = radb.tblRiskAssesses.Where(px => px.Dept == curDeptID).OrderBy(px => px.DateRiskAssess).ToList();
          //  var selcmd = radb.tblRiskAssesses.Where(ra => ra.AppName == appID).ToList(); //the one used for the app name dd change
            var selcmd = radb.tblRiskAssesses.Where(ra => ra.Dept == curDeptID).ToList();
            fvRiskAssess.PageIndex = e.NewPageIndex;
            fvRiskAssess.DataSource = selcmd;         
            fvRiskAssess.DataBind();

     //       Session["curRAID"] = fvRiskAssess.SelectedValue; //stuff current fv record into a session for inserts and updates
            //set the selected index of the app dd for the current page
            RebindDDsForSelIndex((int)fvRiskAssess.SelectedValue);


        }

        protected void fvRiskAssess_DataBound(object sender, EventArgs e)
        {
            Session["curRAID"] = fvRiskAssess.SelectedValue;
        }


        //---------------------New App FV-----------------------------

        protected void fvNewApp_ItemCommand(object sender, FormViewCommandEventArgs e)
        {

        }

        protected void fvNewApp_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            int comp = int.Parse(((DropDownList)fvNewApp.FindControl("ddComp")).SelectedValue);
            String contact = ((TextBox)fvNewApp.FindControl("txtContactName")).Text;
            String appname = ((TextBox)fvNewApp.FindControl("txtAppName")).Text;
            String category = ((DropDownList)fvNewApp.FindControl("ddCategory")).SelectedValue;
            String ExtDataCommRecip = ((TextBox)fvNewApp.FindControl("txtExtDataCommRecip")).Text;
            
            //loop through selected hosps and create a db entry - and save each one individually
           // String Hosps = "";
            ListBox lstHospsBox = (ListBox)fvNewApp.FindControl("lstHosps");
            foreach (ListItem hosp in lstHospsBox.Items)
            {
                if (hosp.Selected)
                {         
                    tblApplication nuApp = new tblApplication();
                                nuApp.ServiceName = appname;
                                nuApp.Company = comp;
                                nuApp.SupportContacts = contact;
                                nuApp.Category = category;
                                nuApp.ExtDataCommRecip = ExtDataCommRecip;
                                nuApp.HospLoc = hosp.Text.ToString();
                                radb.tblApplications.Add(nuApp);
                                radb.SaveChanges();
                }
            }

            //TODO: add the field for the hosps list above and save it...also for updating and displaying

            
            //fvNewApp.ChangeMode(FormViewMode.Insert);

            RebindAppDD(fvRiskAssess.CurrentMode.ToString());

            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('Item Saved');", true);
        }

        protected void fvNewApp_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {

        }

        protected void fvNewApp_ModeChanging(object sender, FormViewModeEventArgs e)
        {
            //handle the mode changing programmatically (automatic when using sqlds)
            fvNewApp.ChangeMode(e.NewMode);
        }

        protected void fvNewComp_ItemCommand(object sender, FormViewCommandEventArgs e)
        {
            

        }

        //---------------------New Comp FV-----------------------------

        protected void fvNewComp_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            String comp = ((TextBox)fvNewComp.FindControl("txtNewCompany")).Text;

            if (string.IsNullOrEmpty(comp))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('Please enter a Company name.');", true);
                return;
            }

            //check company doesn't already exist before adding it
            if (radb.tblCompanies.Any(c => c.CompanyName == comp)) {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('This Company already exists.');", true);
                return; };

            tblCompany nuApp = new tblCompany();
            nuApp.CompanyName = comp;
            radb.tblCompanies.Add(nuApp);
            radb.SaveChanges();

            RebindCompDDAfterInsrt();
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('Company Saved');", true);  

        }

        protected void fvNewComp_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {

        }

        protected void fvNewComp_ModeChanging(object sender, FormViewModeEventArgs e)
        {

        }



        //---------------------Rebinds-----------------------------

        protected void RebindAppDD(string mode)
        {
            DropDownList ddAppNam = null;


            ddAppNam = ((DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt"));

            
            ddAppNam.Items.Clear(); //clear it out before rebinding otherwise it appends if 'append items' is in dd's markup
            ddAppNam.Items.Insert(0, new ListItem("Please Select", ""));

            //nb: the code below for parsing comma separated lists of hosps is no longer technically needed but leaving it in...
            var hosplocs = (from s in radb.tblApplications
                            select new { s.ID, s.HospLoc }).ToList(); // e.g. 1, "svuh,svph"

            var hospAndIDary = new List<Tuple<int, string>> { }; //{ 1, "svuh" }, { 1, "svph" } - populate a new tupple list inside loop below


            foreach (var item in hosplocs)
            {
                //in this outer loop, loop through the 1, "svuh, svph" and split the "svuh,svph" into their own list.
                //in the inner loop, add each id and individual hospital to a tuple

                var hospary = item.HospLoc.Split(',').ToList(); //splits each comma sep'd hosp into it's own row  and add to tuple below

                foreach (var hsp in hospary)
                {
                    //  populate the tuple with current app id and the hospital name - app id will be used for join below
                    hospAndIDary.Add(new Tuple<int, string>(item.ID, hsp));
                }

            }

            hospAndIDary.ToList();
            
            var comp = radb.tblCompanies.SortBy("CompanyName").ToList(); //companies and their ids to be joined to app name list below
            var apps = radb.tblApplications.ToList(); //app names to be joined to companies above radb.tblApplications.SortBy("ServiceName").ToList();
            var compsapps = from co in comp
                            join ap in apps on co.ID equals ap.Company
                             join lstHsps in hospAndIDary on ap.ID equals lstHsps.Item1
                            select new { ID = ap.ID, DisplayText = co.CompanyName + " : " + ap.ServiceName + " : " + lstHsps.Item2};

            
            
            ddAppNam.DataSource = compsapps;
            
            ddAppNam.DataTextField = "DisplayText";
            ddAppNam.DataValueField = "ID";

           // ddAppNam.AppendDataBoundItems = true;
           
            //ddAppNam.DataBind();
           // ddAppNam.AppendDataBoundItems = false;
              
            if (Page.IsPostBack)
            { 
                
              //  ddAppNam.AppendDataBoundItems = false;
                ddAppNam.DataBind();
                
            }
           // if (ddAppNam.Items.Count <= 1) //hack to ensure it binds else it just has 'please select'

            //orig:
            //var AppNamSelCmd = radb.tblApplications.SortBy("ServiceName").ToList();
            //ddAppNam.DataSource = AppNamSelCmd;
            //ddAppNam.DataTextField = "ServiceName";
            //ddAppNam.DataValueField = "ID";     
           // ddAppNam.DataBind();

            
        }

        protected void RebindCompDDAfterInsrt()
        {
            DropDownList ddComp = ((DropDownList)fvNewApp.FindControl("ddComp"));
            

            ddComp.Items.Clear(); //clear it out before rebinding otherwise it appends
            ddComp.Items.Insert(0, new ListItem("Please Select", "")); //re-add the 'please select' to first item

            var CompSelCmd = radb.tblCompanies.SortBy("CompanyName").ToList();

            ddComp.DataSource = CompSelCmd;
            ddComp.DataTextField = "CompanyName";
            ddComp.DataValueField = "ID";
            ddComp.DataBind();
        }

        protected void RebindRiskAssessFvForSelApp(int appID)
        {
           
           var selcmd = radb.tblRiskAssesses.Where(ra => ra.AppName == appID).ToList();
           
            //if there's a record, wire it up (otherwise leave in insert mode) & reset the selectedValue (not index) on app name dd:


            if (selcmd.Count > 0)
            {
                fvRiskAssess.DataSource = selcmd;
                fvRiskAssess.ChangeMode(FormViewMode.Edit);
                fvRiskAssess.DataBind();

                DropDownList ddAppNam = (DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt");
                ddAppNam.SelectedValue = appID.ToString();

                DropDownList ddDept = (DropDownList)fvRiskAssess.FindControl("ddDept");
                ddDept.SelectedIndex = (int)selcmd[0].Dept; //appID.ToString();

            }
            else
            {
                fvRiskAssess.ChangeMode(FormViewMode.Insert);
                fvRiskAssess.DataBind();
                DropDownList ddAppNamInsrt = (DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt");
                ddAppNamInsrt.SelectedValue = appID.ToString();
                
            }

            //Also - get the app category type from the app table and if it's report or spreadsheet, only display section F (by populating a hidden field - jquery pageLoad will take care of the rest)
            var app = radb.tblApplications.Where(ap => ap.ID == appID).Select(b => b.Category).ToList();
            sectionOpn.Value = app[0].ToString();
           // sectionOpn.Value = "Spreadsheets";
        }

        protected void RebindRiskAssessFvForSelDept(int deptID)
        {

            var selcmd = radb.tblRiskAssesses.Where(ra => ra.Dept == deptID).ToList();

            //if there's a record, wire it up (otherwise leave in insert mode) & reset the selectedValue (not index) on app name dd:
            if (selcmd.Count > 0)
            {
                fvRiskAssess.DataSource = selcmd;
                fvRiskAssess.ChangeMode(FormViewMode.Edit);
                fvRiskAssess.DataBind();
                
               // ddDeptFilter.SelectedValue = deptID.ToString();
                
                ////rebind the app name dd (working):
                DropDownList ddAppNam = (DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt");
                int curFVId = (int)fvRiskAssess.SelectedValue;
                var foo = radb.tblRiskAssesses.FirstOrDefault(id => id.ID == curFVId);
                ddAppNam.SelectedValue = foo.AppName.ToString();
                int appID = Int32.Parse(ddAppNam.SelectedValue);
                //filter the fv by the category of the app in the app dd.
                var app = radb.tblApplications.Where(ap => ap.ID == appID).Select(b => b.Category).ToList();
                sectionOpn.Value = app[0].ToString();


                //rebind the dept dd inside the ra form 
                DropDownList ddDept = (DropDownList)fvRiskAssess.FindControl("ddDept");
                ddDept.SelectedValue = foo.Dept.ToString();

            }
            else
            {
                fvRiskAssess.ChangeMode(FormViewMode.Insert);
                fvRiskAssess.DataBind();

                sectionOpn.Value = "";
            }

            

       //     Session["curRAID"] = fvRiskAssess.SelectedValue; //stuff current fv record into a session here for inserts and updates
        }

        protected void RebindDDsForSelIndex(int curFVId)
        {
            // AppNameDD
            DropDownList ddAppNam = (DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt");
            var foo = radb.tblRiskAssesses.FirstOrDefault(id => id.ID == curFVId);
            ddAppNam.SelectedValue = foo.AppName.ToString();
            int appID = Int32.Parse(ddAppNam.SelectedValue);

            //filter the sections in the fv by the category of the app in the app dd.
            var app = radb.tblApplications.Where(ap => ap.ID == appID).Select(b => b.Category).ToList();
            sectionOpn.Value = app[0].ToString();


            //DeptDD
            DropDownList ddDept = (DropDownList)fvRiskAssess.FindControl("ddDept");
            ddDept.SelectedValue = foo.Dept.ToString();


        }



        //---------------------DD's (not incl. dd rebinds)-----------------------------

        protected void ddDept_Init(object sender, EventArgs e)
        {
            DropDownList ddDept = ((DropDownList)fvRiskAssess.FindControl("ddDept"));
            var DeptSelCmd = radb.tblDepartments.SortBy("Department").ToList(); 
            ddDept.DataSource = DeptSelCmd;
            ddDept.DataTextField = "Department";
            ddDept.DataValueField = "ID";

        }


        //init the app name dd
        protected void ddAppNameInsrt_Init(object sender, EventArgs e)
        {

            DropDownList ddAppNam = ((DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt"));

            ddAppNam.Items.Clear(); //clear it out before rebinding otherwise it appends if 'append items' is in dd's markup
            ddAppNam.Items.Insert(0, new ListItem("Please Select", ""));

            //Code below: gets the hospital names (com sep'd) and append them for each item in the list below
            // e.g. Microsoft : SQL Server : (SVUH)
            //      Microsoft : SQL Server : (SVPH)

             
            ////var hosplocs = (from s in radb.tblApplications
            ////                select new {s.ID, s.HospLoc}).ToList(); // e.g. 1, "svuh,svph"
                     
            ////var hospAndIDary = new List<Tuple<int, string>> { }; //{ 1, "foo" }, - populate a new tupple list inside loop below            

            ////foreach (var item in hosplocs)
            ////{
            ////    //in this outer loop, loop through the 1, "svuh, svph" and split the "svuh,svph" into their own list.
            ////    //in the inner loop, add each id and individual hospital to a tuple

            ////    var hospary = item.HospLoc.Split(',').ToList(); //splits each comma sep'd hosp into it's own row  and add to tuple below
                
            ////    foreach (var hsp in hospary)
            ////    {
            ////      //  dicHospsAndIDs.Add(item.ID, hsp);
            ////        hospAndIDary.Add(new Tuple<int,string>(item.ID,hsp));
            ////    }

            ////}

            var comp = radb.tblCompanies.SortBy("CompanyName").ToList(); //companies and their ids to be joined to app name list below
            var apps = radb.tblApplications.ToList(); //app names to be joined to companies above radb.tblApplications.SortBy("ServiceName").ToList();
            var compsapps = from co in comp
                            join ap in apps on co.ID equals ap.Company
                            //join lstHsps in hospAndIDary on ap.ID equals lstHsps.Item1
                            select new { ID = ap.ID, DisplayText = co.CompanyName + " : " + ap.ServiceName + " : " + ap.HospLoc }; //+ lstHsps.Item2  ...+ " : " + hos.Item2 };

            ddAppNam.DataSource = compsapps;

            ddAppNam.DataTextField = "DisplayText";
            ddAppNam.DataValueField = "ID";

        }

        protected void ddAppNameInsrt_SelectedIndexChanged(object sender, EventArgs e)
        {
            //check for any ra records against the selected app name listitem (ra table's 'AppName' int field) and load the ra fv.
            DropDownList ddAppNamInsrt = (DropDownList)fvRiskAssess.FindControl("ddAppNameInsrt");
            string curID = ddAppNamInsrt.SelectedValue;

            if (string.IsNullOrEmpty(curID))
            {
                //user may have switched it back to 'Please Select' so reset fv to insert mode
                fvRiskAssess.ChangeMode(FormViewMode.Insert);
                return;
            } 

           // Session["AppID"] = int.Parse(curID);

            RebindRiskAssessFvForSelApp(int.Parse(curID));
            
            
            
          //   UpdatePanel2.Update(); //main form posted back - required

        }

 

        protected void ddDeptFilter_Init(object sender, EventArgs e)
        {

            var DeptSelCmd = radb.tblDepartments.SortBy("Department").ToList(); 
            ddDeptFilter.DataSource = DeptSelCmd;
            ddDeptFilter.DataTextField = "Department";
            ddDeptFilter.DataValueField = "ID";
            ddDeptFilter.DataBind();
        }

        protected void ddDeptFilter_SelectedIndexChanged(object sender, EventArgs e)
        {

            int curDeptID = Int32.Parse(ddDeptFilter.SelectedValue);  //ddDeptFilter.SelectedIndex;
            RebindRiskAssessFvForSelDept(curDeptID);

            //ddDeptFilter.SelectedValue = curDeptID.ToString();

        }

        protected void ddComp_Init(object sender, EventArgs e)
        {
            DropDownList ddComp = ((DropDownList)fvNewApp.FindControl("ddComp"));
            
            ddComp.Items.Clear(); //clear it out before rebinding otherwise it appends
            ddComp.Items.Insert(0, new ListItem("Please Select", "")); //re-add the 'please select' to first item

            var CompSelCmd = radb.tblCompanies.SortBy("CompanyName").ToList();
            
            ddComp.DataSource = CompSelCmd;
            ddComp.DataTextField = "CompanyName";
            ddComp.DataValueField = "ID";
          
        }

        protected void ddComp_SelectedIndexChanged(object sender, EventArgs e)
        {

        }






    }
}